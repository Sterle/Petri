/***************************************************************************************************
**
** Copyright (c) 2019 Renan Sterle. All rights reserved.
**
** Redistribution and use in source and binary forms, with or without modification, are permitted
** provided that the following conditions are met:
**
**     1. Redistributions of source code must retain the above copyright notice, this list of
** conditions and the following disclaimer.
**     2. Redistributions in binary form must reproduce the above copyright notice, this list of
** conditions and the following disclaimer in the documentation and/or other materials provided with
** the distribution.
**     3. The name of the copyright holder may not be used to endorse or promote products derived
** from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE THE COPYRIGHT HOLDER "AS IS" AND ANY EXPRESS OR IMPLIED
** WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
** BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
***************************************************************************************************/

#include "HostMenu.h"

HostMenu_t::HostMenu_t(QWidget *Parent, Setting_t *Setting) : QWidget(Parent)
{
	//Game settings:
	this->Setting = Setting;

	//Spacer:
	VS1 = new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);

	//Title:
	Title = new QLabel("Host Menu:");
	QFont F = Title->font();
	F.setPointSize(2*F.pointSize());
	F.setBold(true);
	Title->setFont(F);

	//Frame containing entries:
	Frame = new QFrame(this);
	Frame->setMaximumWidth(MenuFrameWidth);
	FrameLayout = new QVBoxLayout(Frame);
	Frame->setFrameStyle(QFrame::StyledPanel | QFrame::Raised);

	//Name entry:
	NameLayout = new QHBoxLayout();
	NameLabel = new QLabel("Name: ", Frame);
	NameInput = new QLineEdit(Frame);
	NameLayout->addWidget(NameLabel);
	NameLayout->addWidget(NameInput);
	FrameLayout->addItem(NameLayout);

	//Slots entry:
	SlotsLayout = new QHBoxLayout();
	SlotsLabel = new QLabel("Slots: ", Frame);
	SlotsInput = new QComboBox(Frame);
	for (int I = 1; I <= MaxSlots; I++)
	{
		SlotsInput->addItem(QString::number(I));
	}
	SlotsInput->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);

	SlotsLayout->addWidget(SlotsLabel);
	SlotsLayout->addWidget(SlotsInput);
	FrameLayout->addItem(SlotsLayout);

	//Bots entry:
	BotsLayout = new QHBoxLayout();
	BotsLabel = new QLabel("Bots: ", Frame);
	BotsInput = new QComboBox(Frame);

	for (int I = 0; I <= MaxBots; I++)
	{
		BotsInput->addItem(QString::number(I));
	}
	BotsInput->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
	BotsLayout->addWidget(BotsLabel);
	BotsLayout->addWidget(BotsInput);
	FrameLayout->addItem(BotsLayout);

	//Size entry:
	SizeLayout = new QHBoxLayout();
	SizeLabel = new QLabel("Size: ", Frame);
	SizeInput = new QComboBox(Frame);

	for (OptionsIterator It = Sizes.begin(); It != Sizes.end(); It++)
	{
		SizeInput->addItem(It->first);
	}
	SizeInput->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
	SizeLayout->addWidget(SizeLabel);
	SizeLayout->addWidget(SizeInput);
	FrameLayout->addItem(SizeLayout);

	//Ressources entry:
	RessourcesLayout = new QHBoxLayout();
	RessourcesLabel = new QLabel("Ressources: ", Frame);
	RessourcesInput = new QComboBox(Frame);

	for (OptionsIterator It = Resources.begin(); It != Resources.end(); It++)
	{
		RessourcesInput->addItem(It->first);
	}
	RessourcesInput->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
	RessourcesLayout->addWidget(RessourcesLabel);
	RessourcesLayout->addWidget(RessourcesInput);
	FrameLayout->addItem(RessourcesLayout);

	//Buttons:
	Start = new QPushButton("Start Game", Frame);
	FrameLayout->addWidget(Start);

	//Spacers:
	VS2 = new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);
	VS3 = new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);

	//Back button:
	Back = new QPushButton("Back", this);
	Back->setMaximumWidth(MenuFrameWidth);
	Back->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);

	//Adds all elements to layout:
	Layout = new QGridLayout(this);
	Layout->addItem(VS1, 0, 0);
	Layout->addWidget(Title, 1, 0, Qt::AlignCenter);
	Layout->addWidget(Frame, 2, 0);
	Layout->addItem(VS2, 3, 0);
	Layout->addItem(VS3, 4, 0);
	Layout->addWidget(Back, 5, 0);

	//Connects slots:
	connect(Back, SIGNAL(clicked()), this, SLOT(BackClicked()));
	connect(Start, SIGNAL(clicked()), this, SLOT(StartClicked()));
}

void HostMenu_t::BackClicked()
{
	emit ChangeState(MainMenu);
}

void HostMenu_t::StartClicked()
{
	Setting->Remote = false;

	//Prepares game name:
	Setting->GameName = NameInput->text().left(MaxNameLength);
	Setting->GameName.remove("\"");
	Setting->GameName.remove("'");
	Setting->GameName.remove("\\");

	Setting->Slots = SlotsInput->currentIndex()+1;
	Setting->Bots = BotsInput->currentIndex();
	Setting->Size = SizeInput->currentIndex();
	Setting->Resources = RessourcesInput->currentIndex();

	if (Setting->GameName.length())
	{
		emit ChangeState(Gameplay);
	}
}

/***************************************************************************************************
**
** Copyright (c) 2019 Renan Sterle. All rights reserved.
**
** Redistribution and use in source and binary forms, with or without modification, are permitted
** provided that the following conditions are met:
**
**     1. Redistributions of source code must retain the above copyright notice, this list of
** conditions and the following disclaimer.
**     2. Redistributions in binary form must reproduce the above copyright notice, this list of
** conditions and the following disclaimer in the documentation and/or other materials provided with
** the distribution.
**     3. The name of the copyright holder may not be used to endorse or promote products derived
** from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE THE COPYRIGHT HOLDER "AS IS" AND ANY EXPRESS OR IMPLIED
** WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
** BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
***************************************************************************************************/

#include "GameFront.h"

//Draws all elements on game frame:
void GameFront_t::paintEvent(QPaintEvent *)
{
	QPainter P(this);
	if (!GreetingReceived) return;

	//Centers and scales painter:
	P.translate(-Zoom*Center+QPoint(width()/2, height()/2));
	P.scale(Zoom, Zoom);

	//Draws dish:
	DrawDish(P);

	//Draws blobs:
	for (std::vector<Blob_t>::iterator B = Blobs.begin(); B != Blobs.end(); B++)
	{
		DrawBlob(*B, P);
	}

	//Draws cells:
	for (std::vector<Cell_t>::iterator C = Cells.begin(); C != Cells.end(); C++)
	{
		DrawCell(*C, P);
	}

	//Resets centralization and scale and draws informations and map:
	P.resetTransform();
	P.scale(1, 1);
	DrawInfo(P, 10);
	DrawMap(P);
}

void GameFront_t::DrawInfo(QPainter &P, int MaxN)
{
	//Sets font:
	QFont F = font();
	F.setBold(true);
	P.setFont(F);
	QFontMetrics FM = P.fontMetrics();

	//Text dimension variables:
	int IndexW = 0, NameW = 0, ScoreW = 0;
	int Height = FM.ascent();

	//Finds widest texts:
	int N = 0;
	for (PlayerIterator R = Ranking.begin(); R != Ranking.end() && N++ < MaxN; R++)
	{
		IndexW = std::max(IndexW, FM.boundingRect(QString::number(N)).width());
		NameW = std::max(NameW, FM.boundingRect(R->Name).width());
		ScoreW = std::max(ScoreW, FM.boundingRect(QString::number(R->Score)).width());
	}

	//Adds font height as a margin:
	IndexW += Height;
	NameW += Height;
	ScoreW += Height;

	//Prints FPS and ranking header:
	N = 0;
	P.setPen(QPen(Qt::black));
	QString FPSString = "FPS: "+QString::number(static_cast<int>(1000 * FPS)) + "Hz";
	P.drawText(QRect(0, 0, Height+FM.boundingRect(FPSString).width(), Height),
			   Qt::AlignLeft | Qt::AlignVCenter | Qt::TextSingleLine, FPSString);
	P.drawText(QRect(0, 2*Height, Height+FM.boundingRect("Ranking:").width(), Height),
			   Qt::AlignLeft | Qt::AlignVCenter | Qt::TextSingleLine, "Ranking:");

	//Prints first MaxN ranking entries:
	for (PlayerIterator R = Ranking.begin(); R != Ranking.end() && N++ < MaxN; R++)
	{
		P.setPen(QPen(R->Color));
		P.drawText(QRect(0, (2+N)*Height, IndexW+NameW, Height),
				   Qt::AlignLeft | Qt::AlignVCenter | Qt::TextSingleLine,
				   QString::number(N) + ". " + R->Name);
		P.drawText(QRect(IndexW+NameW, (2+N)*Height, ScoreW, Height),
				   Qt::AlignRight | Qt::AlignVCenter | Qt::TextSingleLine,
				   QString::number(R->Score));
	}
}

void GameFront_t::DrawDish(QPainter &P)
{
	if (Dish)
	{
		int Radius = Dish->GetRadius();

		//Sets pen and brush:
		P.setPen(QPen(QBrush(Dish->GetColor()), static_cast<double>(Radius*DishBorderRatio)));
		P.setBrush(QBrush(Dish->GetColor().lighter(DishColorFactor)));

		//Draws dish:
		P.drawEllipse(QPoint(0, 0), Radius, Radius);
	}
}

void GameFront_t::DrawBlob(const Blob_t &Blob, QPainter &P)
{
	int Radius = Blob.GetRadius();

	//Sets pen and brush:
	P.setPen(QPen(QBrush(Blob.GetColor()), static_cast<double>(BorderRatio*Radius)));
	P.setBrush(QBrush(Blob.GetColor().lighter(ColorFactor)));

	//Draws blob:
	P.drawEllipse(Blob.GetCenter(), Radius, Radius);
}

void GameFront_t::DrawCell(const Cell_t &Cell, QPainter &P)
{
	ushort Radius = Cell.GetRadius();
	QPoint Center = Cell.GetCenter();
	PlayerStruct &Player = Players[Cell.GetID()];

	//Sets pen and brush:
	P.setPen(QPen(QBrush(Player.Color), static_cast<double>(BorderRatio*Radius),
				  Qt::DashDotLine, Qt::RoundCap));
	P.setBrush(QBrush(Player.Color.lighter(ColorFactor)));

	//Draws rotated cell:
	P.translate(Center);
	P.rotate(static_cast<double>(Cell.GetAngle()));
	P.drawEllipse(QPoint(0, 0), static_cast<int>(Radius*(1.0+Cell.GetEccentricity())), Radius);
	P.rotate(static_cast<double>(-Cell.GetAngle()));
	P.translate(-Center);

	//Sets font:
	QFont F = P.font();
	F.setBold(true);
	QFontMetrics FM(F);
	QString MassString = QString::number(Cell.GetMass());
	int Width = std::max(FM.boundingRect(Player.Name).width(), FM.boundingRect(MassString).width());

	//Sets font dimensions:
	F.setPointSize(std::max(4*Radius*F.pointSize()/Width/3, 1));
	int Height = QFontMetrics(F).ascent();

	//Calculates texts' positions:
	QRect NameStart(Center.x()-Radius, Center.y()-Radius-2*Height/3, 2*Radius, 2*Radius);
	QRect MassStart(Center.x()-Radius, Center.y()-Radius+2*Height/3, 2*Radius, 2*Radius);

	//Draws texts:
	P.setFont(F);
	P.drawText(NameStart, Qt::AlignCenter | Qt::TextSingleLine, Player.Name);
	F.setBold(false);
	P.setFont(F);
	P.drawText(MassStart, Qt::AlignCenter | Qt::TextSingleLine, MassString);
}

void GameFront_t::DrawMap(QPainter &P)
{
	if (Dish)
	{
		//Draws maps:
		int MapRadius = width()/MapWidthFactor;
		QPoint MapCenter(width()-MapRadius, MapRadius);
		P.setPen(QPen(QBrush(Dish->GetColor()), static_cast<double>(2*MapRadius*DishBorderRatio)));
		P.setBrush(QBrush(Dish->GetColor().lighter(DishColorFactor)));
		P.drawEllipse(MapCenter, MapRadius, MapRadius);

		//Draws player's position within map:
		int Radius = MapRadius/MapWidthFactor;
		P.setPen(QPen(QBrush(Setting->Color), static_cast<double>(2*BorderRatio*Radius)));
		P.setBrush(QBrush(Setting->Color.lighter(ColorFactor)));
		P.drawEllipse(MapCenter + MapRadius*Center/Dish->GetRadius(), Radius, Radius);
	}
}

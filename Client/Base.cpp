/***************************************************************************************************
**
** Copyright (c) 2019 Renan Sterle. All rights reserved.
**
** Redistribution and use in source and binary forms, with or without modification, are permitted
** provided that the following conditions are met:
**
**     1. Redistributions of source code must retain the above copyright notice, this list of
** conditions and the following disclaimer.
**     2. Redistributions in binary form must reproduce the above copyright notice, this list of
** conditions and the following disclaimer in the documentation and/or other materials provided with
** the distribution.
**     3. The name of the copyright holder may not be used to endorse or promote products derived
** from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE THE COPYRIGHT HOLDER "AS IS" AND ANY EXPRESS OR IMPLIED
** WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
** BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
***************************************************************************************************/

#include "Base.h"

//Base window constructor:
Base_t::Base_t(QScreen *Screen) : QMainWindow(nullptr)
{
	//Fixed window settings:
	Q_INIT_RESOURCE(Resources);
	setWindowIcon(QIcon(":/Images/Resources/Icon.png"));
	setWindowTitle("Petri");
	setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
	setFixedSize(Width, Height);
	setGeometry(QStyle::alignedRect(Qt::LeftToRight, Qt::AlignCenter, size(),
									Screen->availableGeometry()));

	//Game settings:
	Setting = new Setting_t;

	//Central widget:
	CentralWidget = new QWidget(this);
	setCentralWidget(CentralWidget);
	CentralLayout = new QVBoxLayout(CentralWidget);
	CentralLayout->setContentsMargins(0, 0, 0, 0);

	//Stack and status bar:
	Stack = new QStackedWidget(CentralWidget);
	StatusBar = new QStatusBar(CentralWidget);
	CentralLayout->addWidget(Stack);
	CentralLayout->addWidget(StatusBar);
	StatusBar->hide();
	connect(StatusBar, SIGNAL(messageChanged(const QString&)),
			this, SLOT(StatusChanged(const QString&)));

	//Main menu:
	Menu = new Menu_t(Stack, Setting);
	connect(Menu, SIGNAL(ChangeState(enum State_t)),
			this, SLOT(ChangeState(enum State_t)));
	connect(Menu, SIGNAL(Report(const QString&)),
			this, SLOT(Report(const QString&)));
	Stack->addWidget(Menu);

	//Host menu:
	Host = new HostMenu_t(Stack, Setting);
	connect(Host, SIGNAL(ChangeState(enum State_t)),
			this, SLOT(ChangeState(enum State_t)));
	connect(Host, SIGNAL(Report(const QString&)),
			this, SLOT(Report(const QString&)));
	Stack->addWidget(Host);

	//Join menu:
	Join = new JoinMenu_t(Stack, Setting);
	connect(Join, SIGNAL(ChangeState(enum State_t)),
			this, SLOT(ChangeState(enum State_t)));
	connect(Join, SIGNAL(Report(const QString&)),
			this, SLOT(Report(const QString&)));
	Stack->addWidget(Join);

	//Help menu:
	Help = new HelpMenu_t(Stack);
	connect(Help, SIGNAL(ChangeState(enum State_t)),
			this, SLOT(ChangeState(enum State_t)));
	Stack->addWidget(Help);
}

/*Performs state changes and adapts the active widget, window's size, state and mouse pointer as
 * necessary.*/
void Base_t::ChangeState(enum State_t State)
{
	static State_t Last = MainMenu;
	switch (State)
	{
		case Back:
			//Switches back to last state.
			setFixedSize(Width, Height);
			setWindowState(Qt::WindowMaximized);
			ChangeState(Last);
			break;
		case Closed:
			close();
			break;
		case MainMenu:
			setFixedSize(Width, Height);
			setWindowState(Qt::WindowMaximized);
			Stack->setCurrentWidget(Menu);
			Last = State;
			break;
		case HostMenu:
			Stack->setCurrentWidget(Host);
			Last = State;
			break;
		case JoinMenu:
			Join->EnableDiscovery();
			Stack->setCurrentWidget(Join);
			Last = State;
			break;
		case HelpMenu:
			Stack->setCurrentWidget(Help);
			Last = State;
			break;
		case Gameplay:
			GameFront = new GameFront_t(Stack, Setting);
			connect(GameFront, SIGNAL(ChangeState(enum State_t)),
					this, SLOT(ChangeState(enum State_t)));
			connect(GameFront, SIGNAL(Report(const QString&)), this, SLOT(Report(const QString&)));
			Stack->addWidget(GameFront);
			Stack->setCurrentWidget(GameFront);
			setFixedSize(QWIDGETSIZE_MAX, QWIDGETSIZE_MAX);
			setWindowState(Qt::WindowFullScreen);
			break;
	}
}

//Receives statuses from other emitters and shows them temporarily on the status bar.
void Base_t::Report(const QString &Message)
{
	StatusBar->showMessage(Message, MessageTimeout);
}

//Hides status bar when message disappeas.
void Base_t::StatusChanged(const QString &Message)
{
	StatusBar->setVisible(Message.size());
}

//Base window destructor.
Base_t::~Base_t()
{
	//Deletes independent allocated data.
	delete Setting;
}

/***************************************************************************************************
**
** Copyright (c) 2019 Renan Sterle. All rights reserved.
**
** Redistribution and use in source and binary forms, with or without modification, are permitted
** provided that the following conditions are met:
**
**     1. Redistributions of source code must retain the above copyright notice, this list of
** conditions and the following disclaimer.
**     2. Redistributions in binary form must reproduce the above copyright notice, this list of
** conditions and the following disclaimer in the documentation and/or other materials provided with
** the distribution.
**     3. The name of the copyright holder may not be used to endorse or promote products derived
** from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE THE COPYRIGHT HOLDER "AS IS" AND ANY EXPRESS OR IMPLIED
** WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
** BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
***************************************************************************************************/

#include "GameFront.h"

GameFront_t::GameFront_t(QWidget *Parent, Setting_t *Setting) : QFrame(Parent)
{
	//Initializes members:
	FPS = 0;
	Zoom = 1;
	this->Setting = Setting;
	TimeoutTimer = InputTimer = nullptr;
	Server = nullptr;
	TCPSocket = nullptr;
	UDPSocket = nullptr;
	Dish = nullptr;
	TempFile = nullptr;
	GreetingReceived = SplitCommand = EjectCommand = false;

	//Sets widget attributes:
	setAttribute(Qt::WA_DeleteOnClose);

	//Central title:
	Title = new QLabel(Setting->Remote ? "Connecting..." : "Initializing...");
	QFont F = Title->font();
	F.setPointSize(2*F.pointSize());
	F.setBold(true);
	Title->setFont(F);
	Layout = new QGridLayout(this);
	Layout->addWidget(Title, 0, 0, Qt::AlignCenter);

	/*If mode is remote, simply connects to server to join game. Otherwise, starts server with
	 * specified game settings.*/
	if (Setting->Remote)
	{
		Connect();
	}
	else
	{
		//Retrieves server executable:
		Server = new QProcess();
		QFile ServerFile(":/Server/Resources/PetriServer");
		QString Path = QDir::tempPath()+"/PetriServer";
		ServerFile.copy(Path);
		TempFile = new QFile(Path);
		TempFile->setPermissions(QFile::ReadUser | QFile::ExeUser);

		//Command line arguments:
		QString Slots = "-S " + QString::number(Setting->Slots);
		QString Bots = "-b " + QString::number(Setting->Bots);
		QString Radius = "-r " + QString::number(Sizes[static_cast<ulong>(Setting->Size)].second);
		QString Abundance = "-R " + QString::number(Resources.at(static_cast<ulong>(
																	 Setting->Resources)).second);
		//Connects process' signals:
		connect(Server, SIGNAL(finished(int, QProcess::ExitStatus)),
				this, SLOT(ServerStopped(int, QProcess::ExitStatus)));
		connect(Server, SIGNAL(stateChanged(QProcess::ProcessState)),
				this, SLOT(ServerStateChanged(QProcess::ProcessState)));
		connect(Server, SIGNAL(readyReadStandardOutput()),
				this, SLOT(STDOutReady()));

		//Starts server:
		Server->start(Path,
					  QStringList() << Setting->GameName << Slots << Bots << Radius << Abundance);

		//Prepares timeout observer:
		TimeoutTimer = new QTimer();
		TimeoutTimer->singleShot(Timeout, this, SLOT(LaunchTimedOut()));
	}
}

/*Reads STDOut from server. The first data printed by the server shall be it's TCP port number.
 * Upon receipt, the server is considered ready and the client may connect itself to it.*/
void GameFront_t::STDOutReady()
{
	QString Buffer(Server->readAllStandardOutput());
	bool OK = false;
	Setting->Port = Buffer.toUShort(&OK);
	if (OK)
	{
		Server->disconnect(Server, SIGNAL(readyReadStandardOutput()), this, SLOT(STDOutReady()));
		Connect();
	}
}

void GameFront_t::Connect()
{
	//Initializes sockets:
	TCPSocket = new QTcpSocket();

	//Binds UDP socket:
	UDPSocket = new QUdpSocket();
	if (!UDPSocket->bind())
	{
		emit Report("Could not bind UDP socket.");
		close();
	}

	//Connects signals:
	connect(TCPSocket, SIGNAL(connected()), this, SLOT(Connected()));
	connect(TCPSocket, SIGNAL(readyRead()), this, SLOT(TCPAvailable()));
	connect(UDPSocket, SIGNAL(readyRead()), this, SLOT(UDPAvailable()));
	connect(TCPSocket, SIGNAL(disconnected()), this, SLOT(Disconnected()));
	connect(TCPSocket, SIGNAL(error(QAbstractSocket::SocketError)),
			this, SLOT(SocketError(QAbstractSocket::SocketError)));

	//Connects to server:
	TCPSocket->connectToHost(Setting->Remote ? QHostAddress(Setting->Server)
											 : QHostAddress::LocalHost, Setting->Port);

	//Prepares timeout observer:
	TimeoutTimer = new QTimer();
	TimeoutTimer->singleShot(Timeout, this, SLOT(ConnectionTimedOut()));
}

void GameFront_t::ServerStopped(int, QProcess::ExitStatus)
{
	emit Report("Server stopped unexpectedly.");
	close();
}

//Checks if some abnormal error occured with server's process:
void GameFront_t::ServerStateChanged(QProcess::ProcessState)
{
	if (Server->error() != QProcess::UnknownError)
	{
		emit Report(Server->errorString());
		close();
	}
}

void GameFront_t::ConnectionTimedOut()
{
	if (TCPSocket->state() != QAbstractSocket::ConnectedState)
	{
		emit Report("Connection attempt timed out.");
		close();
	}
}

void GameFront_t::LaunchTimedOut()
{
	if (Server->state() != QProcess::Running)
	{
		emit Report("Server launch attempt timed out.");
		close();
	}
}

//Registers if left or right mouse buttons are pressed.
void GameFront_t::mousePressEvent(QMouseEvent *MouseEvent)
{
	SplitCommand |= (MouseEvent->button() == Qt::LeftButton);
	EjectCommand |= (MouseEvent->button() == Qt::RightButton);
}

//Is called when there is some error on TCP socket. Reports the error upstream.
void GameFront_t::SocketError(QAbstractSocket::SocketError)
{
	emit Report(TCPSocket->errorString());
	close();
}

//Is called when connection is stablished. Starts input timer.
void GameFront_t::Connected()
{
	delete Title;
	InputTimer = new QTimer();
	connect(InputTimer, SIGNAL(timeout()), this, SLOT(SendInputs()));
	InputTimer->setInterval(InputPeriod);
	InputTimer->start();
	setCursor(Qt::CrossCursor);
}

//Is called when connection is broken. Stops input timer and closes game front.
void GameFront_t::Disconnected()
{
	InputTimer->stop();
	close();
}

//Reports sampled inputs to server.
void GameFront_t::SendInputs()
{
	if (GreetingReceived)
	{
		QPoint Mouse = this->mapFromGlobal(QCursor::pos());
		UDPSocket->writeDatagram(ComposeInputReport(ID, Mouse+Center-QPoint(width()/2, height()/2),
									SplitCommand, EjectCommand), TCPSocket->peerAddress(), UDPPort);
	}
	SplitCommand = EjectCommand = false;
}

/*Appends packet to buffer, checks if size is enough to process, checks type,
 extracts data and proceeds accordinly.*/
void GameFront_t::TCPAvailable()
{
	//Append all available data:
	TCPBuffer.append(TCPSocket->readAll());

	//While buffer size is enough to contain length information...
	ushort Length;
	while (static_cast<ulong>(TCPBuffer.length()) >= sizeof(Length))
	{
		//Reads expected length. If the buffer is big enough, reads data.
		Length = *reinterpret_cast<ushort*>(TCPBuffer.data());
		if (TCPBuffer.length() >= Length+static_cast<ushort>(sizeof(Length)))
		{
			TCPBuffer.remove(0, sizeof(Length));
			QDataStream DS(TCPBuffer);

			//Reads type and acts accordingly.
			uchar Type;
			DS >> Type;
			switch (Type)
			{
				case Success:
				case Status:
				{
					QString Message;
					DS >> Message;
					emit Report(Message);
					break;
				}
				case Failure:
				{
					QString Message;
					DS >> Message;
					emit Report(Message);
					close();
					break;
				}
				case ServerGreeting:
				{
					QColor Color, BorderColor;
					DS >> ID >> UDPPort;
					GreetingReceived = true;
					TCPSocket->write(ComposeClientGreeting(ID, UDPSocket->localPort(),
														   Setting->PlayerName, Setting->Color));
					break;
				}
				case DishParameters:
				{
					int Size;
					QColor Color;
					DS >> Size >> Color;
					if (Dish) delete Dish;
					Dish = new Dish_t(Size, Color);
					break;
				}
				default:
					break;
			}
			TCPBuffer.remove(0, Length);
		}
		else
		{
			break;
		}
	}
}

void GameFront_t::UDPAvailable()
{
	//Appends all available data:
	while (UDPSocket->hasPendingDatagrams())
	{
#if QT_VERSION >= QT_VERSION_CHECK(5, 8, 0)
		UDPBuffer.append(UDPSocket->receiveDatagram().data());
#else
		static QByteArray Data;
		int Size = static_cast<int>(UDPSocket->pendingDatagramSize());
		Data.resize(Size);
		UDPSocket->readDatagram(Data.data(), Size);
		UDPBuffer.append(Data.data(), Size);
#endif
	}

	//While buffer size is enough to contain length information...
	ushort Length;
	while (static_cast<ulong>(UDPBuffer.length()) >= sizeof(Length))
	{
		//Reads expected length. If the buffer is big enough, reads data.
		Length = *reinterpret_cast<ushort*>(UDPBuffer.data());
		if (UDPBuffer.length() >= Length+static_cast<ushort>(sizeof(Length)))
		{
			UDPBuffer.remove(0, sizeof(Length));
			QDataStream DS(UDPBuffer);

			//Reads type and acts accordingly.
			uchar Type;
			DS >> Type;
			switch (Type)
			{
				case FrameUpdate:
				{
					const double Alpha = 0.95, Beta = 1.0-Alpha;
					static QTime Last = QTime::currentTime().addMSecs(-InputPeriod);
					static QByteArray Frame;

					Frame.resize(Length-1);
					DS.readRawData(Frame.data(), Length-1);

					DecodeFrame(qUncompress(Frame));
					QTime Now = QTime::currentTime();
					FPS = Alpha*FPS + fmax(Beta/Last.msecsTo(Now), 0);

					Last = Now;

					repaint();
					break;
				}
				default:
					break;
			}
			UDPBuffer.remove(0, Length);
		}
		else
		{
			break;
		}
	}
}

void GameFront_t::DecodeFrame(const QByteArray &Frame)
{
	QDataStream DS(Frame);

	//Gets center and range:
	DS >> Range >> Center;
	//Determines zoom:
	Zoom = fmax(height(), width())/Range;

	ushort N;

	//Clears old map of players and get new ones:
	Players.clear();
	Ranking.clear();
	DS >> N;
	short PID;
	for (int I = 0; I < N; I++)
	{
		DS >> PID;
		PlayerStruct &Player = Players[PID];
		DS >> Player.Color >> Player.Name >> Player.Score;
		Ranking.insert(Player);
	}

	//Clears old vector of blobs and get new ones:
	Blobs.clear();
	DS >> N;
	for (int I = 0; I < N; I++)
	{
		Blobs.push_back(Blob_t());
		DS >> Blobs.back();
	}

	//Clears old vector of cells and get new ones:
	Cells.clear();
	DS >> N;
	for (int I = 0; I < N; I++)
	{
		Cells.push_back(Cell_t());
		DS >> *Cells.rbegin();
	}
}

bool operator<(const PlayerStruct &A, const PlayerStruct &B)
{
	return A.Score > B.Score;
}

//Detects when ESC key is pressed to quit game front widget.
void GameFront_t::keyPressEvent(QKeyEvent *Key)
{
	switch (Key->key())
	{
		case Qt::Key_Escape:
			close();
			break;
		default:
			break;
	}
}

//Destructor of game front widget. Switches back to main menu.
GameFront_t::~GameFront_t()
{
	//Disconnects and frees sockets, if initialized:
	if (TCPSocket && UDPSocket)
	{
		TCPSocket->disconnect();
		TCPSocket->disconnectFromHost();
		TCPSocket->deleteLater();
		emit ChangeState(MainMenu);
		disconnect();
		UDPSocket->deleteLater();
	}

	delete InputTimer;
	delete TimeoutTimer;
	delete Dish;

	//Stops and cleans server:
	if (Server)
	{
		Server->disconnect();
		Server->kill();
		Server->waitForFinished(-1);
		delete Server;
	}

	//Deletes TempFile:
	if (TempFile)
	{
		TempFile->remove();
		delete TempFile;
	}
	emit ChangeState(Back);
}

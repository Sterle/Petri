/***************************************************************************************************
**
** Copyright (c) 2019 Renan Sterle. All rights reserved.
**
** Redistribution and use in source and binary forms, with or without modification, are permitted
** provided that the following conditions are met:
**
**     1. Redistributions of source code must retain the above copyright notice, this list of
** conditions and the following disclaimer.
**     2. Redistributions in binary form must reproduce the above copyright notice, this list of
** conditions and the following disclaimer in the documentation and/or other materials provided with
** the distribution.
**     3. The name of the copyright holder may not be used to endorse or promote products derived
** from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE THE COPYRIGHT HOLDER "AS IS" AND ANY EXPRESS OR IMPLIED
** WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
** BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
***************************************************************************************************/

#include "JoinMenu.h"

JoinMenu_t::JoinMenu_t(QWidget *Parent, Setting_t *Setting) : QWidget(Parent)
{
	//Addresses contains initially broadcast address only:
	Addresses.push_back(QHostAddress(QHostAddress::Broadcast));

	//Game settings:
	this->Setting = Setting;

	//Spacer:
	VS1 = new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);

	//Title:
	Title = new QLabel("Join Menu:");
	QFont F = Title->font();
	F.setPointSize(2*F.pointSize());
	F.setBold(true);
	Title->setFont(F);

	//Frame containing entries:
	Frame = new QFrame(this);
	FrameLayout = new QGridLayout(Frame);
	Frame->setFrameStyle(QFrame::StyledPanel | QFrame::Raised);

	//Servers Tree:
	ServersTree = new QTreeWidget();
	ServersTree->setColumnCount(5);
	ServersTree->setHeaderLabels(
				QStringList() << "Server" << "Port" << "Name" << "Slots" << "Bots");
	ServersTree->setRootIsDecorated(false);
	ServersTree->header()->setSectionResizeMode(QHeaderView::ResizeToContents);
	FrameLayout->addWidget(ServersTree, 0, 0, 1, 2);

	//Manual search entry:
	AddressInput = new QLineEdit();
	QLabel *AddressLabel = new QLabel("Manual search address:");
	FrameLayout->addWidget(AddressLabel, 1, 0, Qt::AlignLeft);
	FrameLayout->addWidget(AddressInput, 1, 1);
	Manual = new QPushButton("Manual Search", Frame);
	Manual->setFixedWidth(MenuFrameWidth);
	FrameLayout->addWidget(Manual, 2, 0, 1, 2, Qt::AlignCenter);

	//Buttons:
	Join = new QPushButton("Join Game", Frame);
	Join->setFixedWidth(MenuFrameWidth);
	FrameLayout->addWidget(Join, 3, 0, 1, 2, Qt::AlignCenter);

	//Spacers:
	VS2 = new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);

	//Back button:
	Back = new QPushButton("Back", this);
	Back->setFixedWidth(MenuFrameWidth);

	//Adds all elements to layout:
	Layout = new QGridLayout(this);
	Layout->addItem(VS1, 0, 0);
	Layout->addWidget(Title, 1, 0, Qt::AlignCenter);
	Layout->addWidget(Frame, 2, 0);
	Layout->addItem(VS2, 3, 0);
	Layout->addWidget(Back, 4, 0, Qt::AlignCenter);

	//Discovery Socket:
	DiscoverySocket = new QUdpSocket();
	DiscoveryTimer = new QTimer();

	//Connects slots:
	connect(Back, SIGNAL(clicked()), this, SLOT(BackClicked()));
	connect(Manual, SIGNAL(clicked()), this, SLOT(ManualClicked()));
	connect(Join, SIGNAL(clicked()), this, SLOT(JoinClicked()));
	connect(DiscoveryTimer, SIGNAL(timeout()), this, SLOT(Discover()));
	connect(ServersTree, SIGNAL(itemDoubleClicked(QTreeWidgetItem*, int)),
			this, SLOT(JoinClicked()));
}

void JoinMenu_t::BackClicked()
{
	DiscoveryTimer->stop();
	DiscoverySocket->disconnect();
	emit ChangeState(MainMenu);
}

void JoinMenu_t::ManualClicked()
{
	Addresses.push_back(QHostAddress(AddressInput->text()));
	AddressInput->clear();
}

void JoinMenu_t::JoinClicked()
{
	QTreeWidgetItem *Item = ServersTree->currentItem();
	if (Item)
	{
		Setting->Remote = true;
		Setting->Server = Item->text(0);
		Setting->Port = Item->text(1).toUShort();
		DiscoveryTimer->stop();
		DiscoverySocket->disconnect();
		emit ChangeState(Gameplay);
	}
}

//Enables broadcast server searches:
void JoinMenu_t::EnableDiscovery()
{
	ServersTree->clear();
	Servers.clear();
	connect(DiscoverySocket, SIGNAL(readyRead()), this, SLOT(Available()));
	DiscoveryTimer->start(DiscoveryPeriod);
}

void JoinMenu_t::Discover()
{
	//Checks older entries and remove deprecated:
	QTime Now = QTime::currentTime();
	for (ServerMap::const_iterator S = Servers.cbegin(); S != Servers.cend();)
	{
		if (S->second.first.msecsTo(Now) > Timeout)
		{
			delete S->second.second;
			S = Servers.erase(S);
		}
		else
		{
			S++;
		}
	}

	//Sends discover message to each address:
	QByteArray Data = ComposeDiscovery();
	for (std::vector<QHostAddress>::iterator A = Addresses.begin(); A != Addresses.end(); A++)
	{
		DiscoverySocket->writeDatagram(Data, *A, StdPort);
	}
}

void JoinMenu_t::Available()
{
	static QByteArray Data;
	static QHostAddress Sender;
	static ushort Port;

	//Reads each datagram:
	while (DiscoverySocket->hasPendingDatagrams())
	{
		int Size = static_cast<int>(DiscoverySocket->pendingDatagramSize());
		Data.resize(Size);
		DiscoverySocket->readDatagram(Data.data(), Size, &Sender, &Port);

		//If data is long enough to contain length information...
		ushort Length;
		if (Size >= static_cast<int>(sizeof(Length)))
		{
			//Reads length and data if data length is sufficient:
			Length = *reinterpret_cast<ushort*>(Data.data());
			QByteArray Buffer(Data.data()+sizeof(Length), Size-static_cast<int>(sizeof(Length)));
			QDataStream DS(&Buffer, QIODevice::ReadOnly);
			if (Buffer.length() >= Length)
			{
				uchar Type;
				DS >> Type;
				switch (Type)
				{
					//It is a server liveness signal:
					case ServerInfo:
					{
						//Reads server information:
						ushort TCPPort, Slots, Players, Bots;
						QString Name;
						DS >> TCPPort >> Name >> Slots >> Players >> Bots;
						ulong ID = (static_cast<ulong>(Sender.toIPv4Address()) << 16)
								   +	static_cast<ulong>(Port);
						Pair Data = std::make_pair(QTime::currentTime(), nullptr);
						ServersIterator R = Servers.insert(std::make_pair(ID, Data));

						if (R.second)
						{
							//New server inserted:
							QStringList Columns;
							bool OK = false;
							QHostAddress SenderIP4(Sender.toIPv4Address(&OK));

							Columns << Sender.toString()
									<< QString::number(TCPPort)
									<< Name
									<< QString::number(Players) + "/" + QString::number(Slots)
									<< QString::number(Bots);

							//Adds to displayed list:
							QTreeWidgetItem *Item = new QTreeWidgetItem(Columns);
							ServersTree->addTopLevelItem(Item);
							R.first->second.second = Item;
						}
						else
						{
							//Updates update time:
							(*(R.first)).second.first = QTime::currentTime();
							//Updates information:
							R.first->second.second->setText(3, QString::number(Players) + "/"
															+ QString::number(Slots));
						}
						break;
					}
					default:
						break;
				}
			}
		}
	}
}

JoinMenu_t::~JoinMenu_t()
{
	DiscoverySocket->disconnect();
	DiscoverySocket->deleteLater();
	DiscoveryTimer->disconnect();
	DiscoveryTimer->deleteLater();
}

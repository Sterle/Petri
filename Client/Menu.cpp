/***************************************************************************************************
**
** Copyright (c) 2019 Renan Sterle. All rights reserved.
**
** Redistribution and use in source and binary forms, with or without modification, are permitted
** provided that the following conditions are met:
**
**     1. Redistributions of source code must retain the above copyright notice, this list of
** conditions and the following disclaimer.
**     2. Redistributions in binary form must reproduce the above copyright notice, this list of
** conditions and the following disclaimer in the documentation and/or other materials provided with
** the distribution.
**     3. The name of the copyright holder may not be used to endorse or promote products derived
** from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE THE COPYRIGHT HOLDER "AS IS" AND ANY EXPRESS OR IMPLIED
** WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
** BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
***************************************************************************************************/

#include "Menu.h"

Menu_t::Menu_t(QWidget *Parent, Setting_t *Setting) : QWidget(Parent)
{
	//Game settings:
	this->Setting = Setting;

	//Spacer:
	VS1 = new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);

	//Title:
	Title = new QLabel("Main Menu:");
	QFont F = Title->font();
	F.setPointSize(2*F.pointSize());
	F.setBold(true);
	Title->setFont(F);

	//Frame containing entries:
	Frame = new QFrame(this);
	Frame->setMaximumWidth(MenuFrameWidth);
	FrameLayout = new QVBoxLayout(Frame);
	Frame->setFrameStyle(QFrame::StyledPanel | QFrame::Raised);

	//Name entry:
	NameLayout = new QHBoxLayout();
	NameLabel = new QLabel("Name: ", Frame);
	NameInput = new QLineEdit("Foo", Frame);
	NameLayout->addWidget(NameLabel);
	NameLayout->addWidget(NameInput);
	FrameLayout->addItem(NameLayout);

	//Color entry:
	ColorLayout = new QHBoxLayout();
	ColorLabel = new QLabel("Color: ", Frame);
	ColorInput = new QComboBox(Frame);
	for (ColorsIterator It = Colors.begin(); It != Colors.end(); It++)
	{
		ColorInput->addItem(It->first);
		ColorInput->setItemData(static_cast<int>(It-Colors.begin()),
								It->second.lighter(ColorFactor), Qt::BackgroundRole);
	}
	ColorInputChanged(ColorInput->currentIndex());
	ColorInput->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
	ColorLayout->addWidget(ColorLabel);
	ColorLayout->addWidget(ColorInput);
	FrameLayout->addItem(ColorLayout);

	//Buttons:
	Join = new QPushButton("Join Game", Frame);
	Host = new QPushButton("Host Game", Frame);
	Help = new QPushButton("Help", Frame);
	FrameLayout->addWidget(Join);
	FrameLayout->addWidget(Host);
	FrameLayout->addWidget(Help);

	//Spacers:
	VS2 = new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);
	VS3 = new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);

	//Quit button:
	Quit = new QPushButton("Quit", this);
	Quit->setMaximumWidth(MenuFrameWidth);
	Quit->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);

	//Adds all elements to layout:
	Layout = new QGridLayout(this);
	Layout->addItem(VS1, 0, 0);
	Layout->addWidget(Title, 1, 0, Qt::AlignCenter);
	Layout->addWidget(Frame, 2, 0);
	Layout->addItem(VS2, 3, 0);
	Layout->addItem(VS3, 4, 0);
	Layout->addWidget(Quit, 5, 0);

	//Connects slots:
	connect(ColorInput, SIGNAL(currentIndexChanged(int)), this, SLOT(ColorInputChanged(int)));
	connect(Quit, SIGNAL(clicked()), this, SLOT(QuitClicked()));
	connect(Join, SIGNAL(clicked()), this, SLOT(JoinClicked()));
	connect(Host, SIGNAL(clicked()), this, SLOT(HostClicked()));
	connect(Help, SIGNAL(clicked()), this, SLOT(HelpClicked()));
}

//Sets color of selected color:
void Menu_t::ColorInputChanged(int Index)
{
	ColorInput->setPalette(QPalette(Colors[static_cast<ulong>(Index)].second));
}

void Menu_t::QuitClicked()
{
	emit ChangeState(Closed);
}

void Menu_t::GetSettings()
{
	Setting->PlayerName = NameInput->text().left(MaxNameLength);
	Setting->Color = Colors[static_cast<ulong>(ColorInput->currentIndex())].second;
	Setting->LightColor = Setting->Color.lighter(ColorFactor);
}

void Menu_t::JoinClicked()
{
	GetSettings();
	emit ChangeState(JoinMenu);
}

void Menu_t::HostClicked()
{
	GetSettings();
	emit ChangeState(HostMenu);
}

void Menu_t::HelpClicked()
{
	GetSettings();
	emit ChangeState(HelpMenu);
}

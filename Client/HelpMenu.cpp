/***************************************************************************************************
**
** Copyright (c) 2019 Renan Sterle. All rights reserved.
**
** Redistribution and use in source and binary forms, with or without modification, are permitted
** provided that the following conditions are met:
**
**     1. Redistributions of source code must retain the above copyright notice, this list of
** conditions and the following disclaimer.
**     2. Redistributions in binary form must reproduce the above copyright notice, this list of
** conditions and the following disclaimer in the documentation and/or other materials provided with
** the distribution.
**     3. The name of the copyright holder may not be used to endorse or promote products derived
** from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE THE COPYRIGHT HOLDER "AS IS" AND ANY EXPRESS OR IMPLIED
** WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
** BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
***************************************************************************************************/

#include "HelpMenu.h"

HelpMenu_t::HelpMenu_t(QWidget *Parent) : QWidget(Parent)
{
	//Spacer:
	VS1 = new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);

	//Title:
	Title = new QLabel("Help:");
	QFont F = Title->font();
	F.setPointSize(2*F.pointSize());
	F.setBold(true);
	Title->setFont(F);

	//Frame containing entries:
	Frame = new QFrame(this);
	Frame->setMaximumWidth(MenuFrameWidth);
	FrameLayout = new QVBoxLayout(Frame);
	Frame->setFrameStyle(QFrame::StyledPanel | QFrame::Raised);

	//Adds help message:
	QString HelpMessage = "Controls:\n"
						  "  -Move: Point using mouse;\n"
						  "  -Trigger mitosis: Mouse left-click;\n"
						  "  -Eject mass: Mouse right-click;";
	FrameLayout->addWidget(new QLabel(HelpMessage, Frame));

	//Spacers:
	VS2 = new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);
	VS3 = new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);

	//Back button:
	Back = new QPushButton("Back", this);
	Back->setMaximumWidth(MenuFrameWidth);
	Back->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);

	//Add all elements to layout:
	Layout = new QGridLayout(this);
	Layout->addItem(VS1, 0, 0);
	Layout->addWidget(Title, 1, 0, Qt::AlignCenter);
	Layout->addWidget(Frame, 2, 0);
	Layout->addItem(VS2, 3, 0);
	Layout->addItem(VS3, 4, 0);
	Layout->addWidget(Back, 5, 0);

	//Connect slots:
	connect(Back, SIGNAL(clicked()), this, SLOT(BackClicked()));
}

void HelpMenu_t::BackClicked()
{
	emit ChangeState(MainMenu);
}

/***************************************************************************************************
**
** Copyright (c) 2019 Renan Sterle. All rights reserved.
**
** Redistribution and use in source and binary forms, with or without modification, are permitted
** provided that the following conditions are met:
**
**     1. Redistributions of source code must retain the above copyright notice, this list of
** conditions and the following disclaimer.
**     2. Redistributions in binary form must reproduce the above copyright notice, this list of
** conditions and the following disclaimer in the documentation and/or other materials provided with
** the distribution.
**     3. The name of the copyright holder may not be used to endorse or promote products derived
** from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE THE COPYRIGHT HOLDER "AS IS" AND ANY EXPRESS OR IMPLIED
** WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
** BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
***************************************************************************************************/

#ifndef GAMEFRONT_H
#define GAMEFRONT_H

//Qt headers:
#include <QFrame>
#include <QKeyEvent>
#include <QTcpSocket>
#include <QUdpSocket>
#include <QHostAddress>
#include <QTimer>
#include <QPainter>
#include <QTime>
#include <QLayout>
#include <QLabel>

//Compatibility header:
#if QT_VERSION >= QT_VERSION_CHECK(5, 8, 0)
#include <QNetworkDatagram>
#endif

//STD header:
#include <set>

//Project headers:
#include "Common.h"
#include "Network.h"
#include "Elements.h"
#include "Player.h"

//Player information struct:
struct PlayerStruct
{
	QColor Color;
	QString Name;
	ushort Score;
};

bool operator<(const PlayerStruct &A, const PlayerStruct &B);

//Game interface class:
class GameFront_t : public QFrame
{
		Q_OBJECT
	public:
		GameFront_t(QWidget *Parent, Setting_t *Setting);
		~GameFront_t() override;
	private slots:
		void keyPressEvent(QKeyEvent *Key) override;
		void mousePressEvent(QMouseEvent *MouseEvent) override;
		void paintEvent(QPaintEvent*) override;

		void ConnectionTimedOut();
		void LaunchTimedOut();
		void SocketError(QAbstractSocket::SocketError);
		void Connected();
		void Disconnected();
		void SendInputs();
		void TCPAvailable();
		void UDPAvailable();
		void ServerStopped(int, QProcess::ExitStatus);
		void ServerStateChanged(QProcess::ProcessState);
		void STDOutReady();
	signals:
		void ChangeState(enum State_t State);
		void Report(const QString &Message);
	private:
		typedef std::multiset<PlayerStruct>::iterator PlayerIterator;
		const int MapWidthFactor = 20;
		const int InputPeriod = 40;
		const int Timeout = 5000;
		const unsigned long TrialPause = 200;

		QGridLayout *Layout;
		QLabel *Title;
		Setting_t *Setting;
		QTcpSocket *TCPSocket;
		QUdpSocket *UDPSocket;
		QFile *TempFile;
		QProcess *Server;
		QTimer *InputTimer, *TimeoutTimer;
		QByteArray TCPBuffer, UDPBuffer;

		short ID;
		ushort UDPPort;
		bool GreetingReceived;

		bool SplitCommand, EjectCommand;

		double Range, Zoom, FPS;
		QPoint Center;
		Dish_t *Dish;
		std::vector<Blob_t> Blobs;
		std::vector<Cell_t> Cells;
		std::map<short, PlayerStruct> Players;
		std::multiset<PlayerStruct> Ranking;

		void Connect();
		void DecodeFrame(const QByteArray &Frame);
		void DrawDish(QPainter &P);
		void DrawBlob(const Blob_t &Blob, QPainter &P);
		void DrawCell(const Cell_t &Cell, QPainter &P);
		void DrawInfo(QPainter &P, int MaxN);
		void DrawMap(QPainter &P);
};

#endif

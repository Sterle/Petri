/***************************************************************************************************
**
** Copyright (c) 2019 Renan Sterle. All rights reserved.
**
** Redistribution and use in source and binary forms, with or without modification, are permitted
** provided that the following conditions are met:
**
**     1. Redistributions of source code must retain the above copyright notice, this list of
** conditions and the following disclaimer.
**     2. Redistributions in binary form must reproduce the above copyright notice, this list of
** conditions and the following disclaimer in the documentation and/or other materials provided with
** the distribution.
**     3. The name of the copyright holder may not be used to endorse or promote products derived
** from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE THE COPYRIGHT HOLDER "AS IS" AND ANY EXPRESS OR IMPLIED
** WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
** BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
***************************************************************************************************/

#ifndef COMMON_H
#define COMMON_H

//Qt header:
#include <QColor>

//STD headers:
#include <cmath>
#include <vector>

//Base window states:
enum State_t
{
	MainMenu,
	JoinMenu,
	HostMenu,
	HelpMenu,
	Gameplay,
	Back,
	Closed
};

//Constants:
const std::vector<std::pair<QString, QColor> > Colors =
{
	std::make_pair("Cyan", QColor(0, 0x80, 0x80)),
	std::make_pair("Magenta", QColor(0x80, 0, 0x80)),
	std::make_pair("Yellow", QColor(0x80, 0x80, 0)),
	std::make_pair("Red", QColor(0xB5, 0, 0)),
	std::make_pair("Green", QColor(0, 0xB5, 0)),
	std::make_pair("Blue", QColor(0, 0, 0xB5)),
	std::make_pair("Gray", QColor(0x68, 0x68, 0x68))
};
const double TwoPi = 8*atan(1.0);
const QColor DishBorderColor = Qt::blue;
const QColor DishColor = DishBorderColor.lighter(185);

//Predefined dish sizes:
const std::vector<std::pair<QString, int>> Sizes =
{
	std::make_pair("Small", 800),
	std::make_pair("Medium", 1600),
	std::make_pair("Large", 3200)
};

//Predefined resource abundance levels:
const std::vector<std::pair<QString, int>> Resources =
{
	std::make_pair("Scarce", 100),
	std::make_pair("Medium", 300),
	std::make_pair("Plenty", 900)
};

//Join and host game settings:
struct Setting_t
{
	bool Remote;

	//Join fields:
	QString PlayerName;
	QColor Color, LightColor;
	QString Server;
	ushort Port;
	//Host fields:
	QString GameName;
	int Size, Bots, Slots, Resources;
};

//Standard parameters:
const int MaxBots = 5, MaxSlots = 10;
const int MenuFrameWidth = 300;
const int MaxNameLength = 10;
const int MessageTimeout = 2000;
const ushort StdPort = 54000;

//Drawing constants:
const int ColorFactor = 200, DishColorFactor = 220;
constexpr static double BorderRatio = 0.1;
constexpr static double DishBorderRatio = 0.01;

#endif

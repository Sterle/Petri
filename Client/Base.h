/***************************************************************************************************
**
** Copyright (c) 2019 Renan Sterle. All rights reserved.
**
** Redistribution and use in source and binary forms, with or without modification, are permitted
** provided that the following conditions are met:
**
**     1. Redistributions of source code must retain the above copyright notice, this list of
** conditions and the following disclaimer.
**     2. Redistributions in binary form must reproduce the above copyright notice, this list of
** conditions and the following disclaimer in the documentation and/or other materials provided with
** the distribution.
**     3. The name of the copyright holder may not be used to endorse or promote products derived
** from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE THE COPYRIGHT HOLDER "AS IS" AND ANY EXPRESS OR IMPLIED
** WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
** BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
***************************************************************************************************/

#ifndef MAIN_H
#define MAIN_H

//Qt headers:
#include <QMainWindow>
#include <QStyle>
#include <QScreen>
#include <QStackedWidget>
#include <QStatusBar>

//Project headers:
#include "Common.h"
#include "Menu.h"
#include "HostMenu.h"
#include "JoinMenu.h"
#include "GameFront.h"
#include "HelpMenu.h"

//Base window:
class Base_t : public QMainWindow
{
		Q_OBJECT
	public:
		Base_t(QScreen *Screen);
		~Base_t();
	private slots:
		void ChangeState(enum State_t State);
		void Report(const QString &Message);
		void StatusChanged(const QString &Message);
	private:
		const int Width = 640;
		const int Height = 480;

		QWidget *CentralWidget;
		QVBoxLayout *CentralLayout;

		QStackedWidget *Stack;
		QStatusBar *StatusBar;

		Menu_t *Menu;
		HostMenu_t *Host;
		JoinMenu_t *Join;
		HelpMenu_t *Help;
		GameFront_t *GameFront;

		State_t Status = MainMenu;
		Setting_t *Setting;
};

#endif

QT	+= core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET	= PetriClient
TEMPLATE= app
DEFINES	+= QT_DEPRECATED_WARNINGS
CONFIG	+= c++11 static optimize_full release

SOURCES	+= \
	Base.cpp \
	Elements.cpp \
    HelpMenu.cpp \
    HostMenu.cpp \
    JoinMenu.cpp \
	Main.cpp \
	GameFront.cpp \
	GameFrontGraphics.cpp \
	Menu.cpp \
	Network.cpp \
	Player.cpp \
    RandomGenerator.cpp

HEADERS	+= \
	Base.h \
	Common.h \
Elements.h \
	GameFront.h \
    HelpMenu.h \
    HostMenu.h \
    JoinMenu.h \
	Menu.h \
	Network.h \
    Player.h \
    Player.h \
    RandomGenerator.h

RESOURCES	+= \
Resources.qrc

FORMS +=

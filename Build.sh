#!/bin/bash

Root=$(pwd)"/"
Build="Build";
Client="Client";
Server="Server";
ClientBuild="Build/Client";
ServerBuild="Build/Server";

mkdir -p $ClientBuild
mkdir -p $ServerBuild


echo "Root directory: $Root"
echo "Using: "$(qmake -v);

cd $Root$Server
qmake "-o" "$Root$ServerBuild/Makefile"

cd $Root$ServerBuild
make
mv PetriServer ..


cd "$Root$Client/Resources"
ln -sf ../../Build/PetriServer PetriServer

cd $Root$Client
qmake "-o" "$Root$ClientBuild/Makefile"
echo $Root$ClientBuild
cd $Root$ClientBuild
make
mv PetriClient ..

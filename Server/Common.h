/***************************************************************************************************
**
** Copyright (c) 2019 Renan Sterle. All rights reserved.
**
** Redistribution and use in source and binary forms, with or without modification, are permitted
** provided that the following conditions are met:
**
**     1. Redistributions of source code must retain the above copyright notice, this list of
** conditions and the following disclaimer.
**     2. Redistributions in binary form must reproduce the above copyright notice, this list of
** conditions and the following disclaimer in the documentation and/or other materials provided with
** the distribution.
**     3. The name of the copyright holder may not be used to endorse or promote products derived
** from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE THE COPYRIGHT HOLDER "AS IS" AND ANY EXPRESS OR IMPLIED
** WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
** BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
***************************************************************************************************/

#ifndef COMMON_H
#define COMMON_H

//Qt headers:
#include <QtGlobal>
#include <QColor>

//STD headers:
#include <cmath>
#include <list>
#include <vector>

const std::vector<std::pair<QString, QColor> > Colors =
{
	std::make_pair("Cyan", QColor(0, 0x80, 0x80)),
	std::make_pair("Magenta", QColor(0x80, 0, 0x80)),
	std::make_pair("Yellow", QColor(0x80, 0x80, 0)),
	std::make_pair("Red", QColor(0xB5, 0, 0)),
	std::make_pair("Green", QColor(0, 0xB5, 0)),
	std::make_pair("Blue", QColor(0, 0, 0xB5)),
	std::make_pair("Gray", QColor(0x68, 0x68, 0x68))
};

//Standard parameters and limits:
const int StdRadius = 400, MinRadius = 400, MaxRadius = 4000;
const int StdSlots = 1, MinSlots = 1, MaxSlots = 10;
const short StdBots = 2, MinBots = 0, MaxBots = 10;
const int StdRessources = 300, MinRessources = 1, MaxRessources = 1000;
const int StdMaxBlobs = 500, MinMaxBlobs = 1, MaxMaxBlobs = 1000;
const short StdColor = 5, MaxColor = static_cast<short>(Colors.size()), MinColor = 0;
const int StdPort = 54000;

//Constants:
const double TwoPi = 8*atan(1.0);

struct Setting_t
{
		QString Name;
		bool LocalMode;
		ushort Port;
		ushort Radius;
		ushort Slots;
		ushort Bots;
		ushort Ressources;
		ulong MaxBlobs;
		ushort Color;
};

#endif

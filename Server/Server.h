/***************************************************************************************************
**
** Copyright (c) 2019 Renan Sterle. All rights reserved.
**
** Redistribution and use in source and binary forms, with or without modification, are permitted
** provided that the following conditions are met:
**
**     1. Redistributions of source code must retain the above copyright notice, this list of
** conditions and the following disclaimer.
**     2. Redistributions in binary form must reproduce the above copyright notice, this list of
** conditions and the following disclaimer in the documentation and/or other materials provided with
** the distribution.
**     3. The name of the copyright holder may not be used to endorse or promote products derived
** from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE THE COPYRIGHT HOLDER "AS IS" AND ANY EXPRESS OR IMPLIED
** WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
** BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
***************************************************************************************************/

#ifndef SERVER_H
#define SERVER_H

//Qt headers:
#include <QTcpServer>
#include <QTcpSocket>
#include <QUdpSocket>
#include <QNetworkInterface>
#include <QPoint>

//Compatibility header:
#if QT_VERSION >= QT_VERSION_CHECK(5, 8, 0)
#include <QNetworkDatagram>
#endif

//STD header:
#include <iostream>

//Project headers:
#include "Common.h"
#include "Network.h"
#include "Client.h"
#include "Elements.h"

//Server class:
class Server_t : public QTcpServer
{
		Q_OBJECT
	public:
		explicit Server_t(Setting_t &Settings, QObject *Parent = nullptr);
		~Server_t();
	private slots:
		void SocketError(QAbstractSocket::SocketError);
		void ListClient();
		void RemoveClient();
		void UDPAvailable();
		void TCPAvailable();
		void SendFrame(short ID, QByteArray &Frame);
		void Message(short ID, Type_t Type, const QString &Content);
		void SendDishParameters(short ID, Dish_t *Dish);
	signals:
		void JoinRequest(short ID, const QString &Name, const QColor &Color);
		void ClientRemoved(short ID);
		void UpdateInput(short ID, const QPoint &Input, bool Split, bool Eject);
	private:
		Setting_t &Settings;
		short NextID;
		std::map<short, Client_t*> Clients;
		QUdpSocket *UDPSocket;
		QByteArray UDPBuffer;
		bool FindClient(QTcpSocket *Socket, short &ID);
};

#endif

/***************************************************************************************************
**
** Copyright (c) 2019 Renan Sterle. All rights reserved.
**
** Redistribution and use in source and binary forms, with or without modification, are permitted
** provided that the following conditions are met:
**
**     1. Redistributions of source code must retain the above copyright notice, this list of
** conditions and the following disclaimer.
**     2. Redistributions in binary form must reproduce the above copyright notice, this list of
** conditions and the following disclaimer in the documentation and/or other materials provided with
** the distribution.
**     3. The name of the copyright holder may not be used to endorse or promote products derived
** from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE THE COPYRIGHT HOLDER "AS IS" AND ANY EXPRESS OR IMPLIED
** WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
** BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
***************************************************************************************************/

#include "RandomGenerator.h"

#if QT_VERSION < QT_VERSION_CHECK(5, 10, 0)
using namespace std;

QRandomGenerator::QRandomGenerator(uint Seed)
{
	Engine = mt19937_64(Seed);
}

uint32_t QRandomGenerator::generate()
{
	return static_cast<uint32_t>(generate64() >> 16);
}

uint64_t QRandomGenerator::generate64()
{
	return Engine();
}

double QRandomGenerator::generateDouble()
{
	// IEEE 754 double precision has:
	//   1 bit      sign
	//  10 bits     exponent
	//  53 bits     mantissa
	// In order for our result to be normalized in the range [0, 1), we
	// need exactly 53 bits of random data. Use generate64() to get enough.
	uint64_t x = generate64();
	uint64_t limit = uint64_t(1) << std::numeric_limits<double>::digits;
	x >>= std::numeric_limits<uint64_t>::digits - std::numeric_limits<double>::digits;
	return double(x) / double(limit);
}
#endif

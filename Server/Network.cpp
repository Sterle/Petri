/***************************************************************************************************
**
** Copyright (c) 2019 Renan Sterle. All rights reserved.
**
** Redistribution and use in source and binary forms, with or without modification, are permitted
** provided that the following conditions are met:
**
**     1. Redistributions of source code must retain the above copyright notice, this list of
** conditions and the following disclaimer.
**     2. Redistributions in binary form must reproduce the above copyright notice, this list of
** conditions and the following disclaimer in the documentation and/or other materials provided with
** the distribution.
**     3. The name of the copyright holder may not be used to endorse or promote products derived
** from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE THE COPYRIGHT HOLDER "AS IS" AND ANY EXPRESS OR IMPLIED
** WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
** BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
***************************************************************************************************/

#include "Network.h"

//Composes server discovery message:
QByteArray ComposeDiscovery()
{
	QByteArray A;
	QDataStream S(&A, QIODevice::WriteOnly);

	S << static_cast<uchar>(Discovery);

	return A;
}

//Composes server discovery reply message:
QByteArray ComposeServerInfo(ushort TCPPort, const QString &Name, ushort Slots, ushort Players,
							 ushort Bots)
{
	QByteArray A;
	QDataStream S(&A, QIODevice::WriteOnly);

	S << static_cast<uchar>(ServerInfo) << TCPPort << Name << Slots << Players << Bots;

	ushort Length = static_cast<ushort>(A.length());
	A.prepend(reinterpret_cast<char*>(&Length), sizeof(Length));
	return A;
}

//Composes server greeting message:
QByteArray ComposeServerGreeting(short ID, ushort UDPPort)
{
	QByteArray A;
	QDataStream S(&A, QIODevice::WriteOnly);

	S << static_cast<uchar>(ServerGreeting) << ID << UDPPort;

	ushort Length = static_cast<ushort>(A.length());
	A.prepend(reinterpret_cast<char*>(&Length), sizeof(Length));
	return A;
}

//Composes client greeting message:
QByteArray ComposeClientGreeting(short ID, ushort UDPPort, const QString &Name, const QColor &Color)
{
	QByteArray A;
	QDataStream S(&A, QIODevice::WriteOnly);

	S << static_cast<uchar>(ClientGreeting) << ID << UDPPort << Name << Color;

	ushort Length = static_cast<ushort>(A.length());
	A.prepend(reinterpret_cast<char*>(&Length), sizeof(Length));
	return A;
}

//Composes information messages message:
QByteArray ComposeMessage(Type_t Type, const QString Content)
{
	QByteArray A;
	QDataStream S(&A, QIODevice::WriteOnly);

	S << static_cast<uchar>(Type) << Content;

	ushort Length = static_cast<ushort>(A.length());
	A.prepend(reinterpret_cast<char*>(&Length), sizeof(Length));
	return A;
}

//Composes player input report message:
QByteArray ComposeInputReport(short ID, const QPoint &Input, bool Split, bool Eject)
{
	QByteArray A;
	QDataStream S(&A, QIODevice::WriteOnly);

	uchar Flags = (Split ? SplitMask : 0) + (Eject ? EjectMask : 0);
	S << static_cast<uchar>(InputReport) << ID << Input << Flags;

	ushort Length = static_cast<ushort>(A.length());
	A.prepend(reinterpret_cast<char*>(&Length), sizeof(Length));

	return A;
}

//Composes dish information message:
QByteArray ComposeDishParameters(int Size, const QColor &Color)
{
	QByteArray A;
	QDataStream S(&A, QIODevice::WriteOnly);

	S << static_cast<uchar>(DishParameters) << Size << Color;

	ushort Length = static_cast<ushort>(A.length());
	A.prepend(reinterpret_cast<char*>(&Length), sizeof(Length));
	return A;
}

//Adjusts frame prepending type and length:
QByteArray AdjustFrame(QByteArray &Frame)
{
	QByteArray A;
	QDataStream S(&A, QIODevice::WriteOnly);
	S << static_cast<uchar>(FrameUpdate);

	Frame.prepend(A);
	ushort Length = static_cast<ushort>(Frame.length());
	Frame.prepend(reinterpret_cast<char*>(&Length), sizeof(Length));

	return Frame;
}

/***************************************************************************************************
**
** Copyright (c) 2019 Renan Sterle. All rights reserved.
**
** Redistribution and use in source and binary forms, with or without modification, are permitted
** provided that the following conditions are met:
**
**     1. Redistributions of source code must retain the above copyright notice, this list of
** conditions and the following disclaimer.
**     2. Redistributions in binary form must reproduce the above copyright notice, this list of
** conditions and the following disclaimer in the documentation and/or other materials provided with
** the distribution.
**     3. The name of the copyright holder may not be used to endorse or promote products derived
** from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE THE COPYRIGHT HOLDER "AS IS" AND ANY EXPRESS OR IMPLIED
** WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
** BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
***************************************************************************************************/

#include "Main.h"

//Global variables:
static QCoreApplication *Application;
static Server_t *Server;
static Game_t *Game;

int main(int ArgC, char *ArgV[])
{
	Application = new QCoreApplication(ArgC, ArgV);
	QCoreApplication::setApplicationName("PetriServer");

	//Parses settings and starts server:
	Setting_t Settings;
	if (!ParseParameters(*Application, Settings)) return EXIT_FAILURE;
	try
	{
		Server = new Server_t(Settings);
	}
	catch (const char *Error)
	{
		qDebug() << Error;
		delete Application;
		return EXIT_FAILURE;
	}

	//Starts game:
	Game = new Game_t(Settings);

	//Connects signals:
	signal(SIGINT, Exit);
	signal(SIGTERM, Exit);
	QObject::connect(Game, SIGNAL(SendFrame(short, QByteArray&)),
					 Server, SLOT(SendFrame(short, QByteArray&)));
	QObject::connect(Server, SIGNAL(JoinRequest(short, const QString&, const QColor&)),
					 Game, SLOT(JoinRequest(short, const QString&, const QColor&)));
	QObject::connect(Game, SIGNAL(SendDishParameters(short, Dish_t*)),
					 Server, SLOT(SendDishParameters(short, Dish_t*)));
	QObject::connect(Server, SIGNAL(ClientRemoved(short)),
					 Game, SLOT(ClientRemoved(short)));
	QObject::connect(Game, SIGNAL(Message(short, Type_t, const QString&)),
					 Server, SLOT(Message(short, Type_t, const QString&)));
	QObject::connect(Server, SIGNAL(UpdateInput(short, const QPoint&, bool, bool)),
					 Game, SLOT(UpdateInput(short, const QPoint&, bool, bool)));

	//Starts event loop:
	return Application->exec();
}

bool ParseParameters(QCoreApplication &Application, Setting_t &Settings)
{
	QCommandLineParser Parser;
	Parser.addHelpOption();

	Parser.addPositionalArgument("Name", "Server Name");
	QCommandLineOption LocalOption(QStringList() << "l" << "local");
	Parser.addOption(LocalOption);

	QCommandLineOption PortOption(QStringList() << "p" << "port");
	PortOption.setValueName("Port");
	Parser.addOption(PortOption);

	QCommandLineOption RadiusOption(QStringList() << "r" << "radius");
	RadiusOption.setValueName("Dish's radius");
	Parser.addOption(RadiusOption);

	QCommandLineOption SlotsOption(QStringList() << "S" << "slots");
	SlotsOption.setValueName("Number of player slots");
	Parser.addOption(SlotsOption);

	QCommandLineOption BotsOption(QStringList() << "b" << "bots");
	BotsOption.setValueName("Number of bots");
	Parser.addOption(BotsOption);

	QCommandLineOption ResourcesOption(QStringList() << "R" << "resources");
	ResourcesOption.setValueName("Ressources availability (blobs per minute)");
	Parser.addOption(ResourcesOption);

	QCommandLineOption MaxBlobsOption(QStringList() << "m" << "maxb");
	MaxBlobsOption.setValueName("Maximum number of blobs");
	Parser.addOption(MaxBlobsOption);

	QCommandLineOption ColorOption(QStringList() << "c" << "color");
	ColorOption.setValueName("Index of dish color.");
	Parser.addOption(ColorOption);

	Parser.process(Application);

	if (!Parser.positionalArguments().size())
	{
		qDebug("Server name not provided.");
		return false;
	}

	Settings.Name = Parser.positionalArguments().at(0);
	Settings.LocalMode = Parser.isSet(LocalOption);
	Settings.Port = StdPort;
	Settings.Radius = StdRadius;
	Settings.Slots = StdSlots;
	Settings.Bots = StdBots;
	Settings.Ressources = StdRessources;
	Settings.MaxBlobs = StdMaxBlobs;
	Settings.Color = StdColor;

	bool Ok;
	if (Parser.isSet(PortOption))
	{
		Settings.Port = Parser.value(PortOption).toUShort(&Ok);
		if (!Ok)
		{
			qDebug("Invalid port number.");
			return false;
		}
	}

	if (Parser.isSet(RadiusOption))
	{
		Settings.Radius = Parser.value(RadiusOption).toUShort(&Ok);
		if (!Ok || static_cast<short>(Settings.Radius) < MinRadius || Settings.Radius > MaxRadius)
		{
			qDebug("Invalid radius.");
			return false;
		}
	}

	if (Parser.isSet(SlotsOption))
	{
		Settings.Slots = Parser.value(SlotsOption).toUShort(&Ok);
		if (!Ok || static_cast<short>(Settings.Bots) < MinSlots || Settings.Bots > MaxSlots)
		{
			qDebug("Invalid number of player slots.");
			return false;
		}
	}

	if (Parser.isSet(BotsOption))
	{
		Settings.Bots = Parser.value(BotsOption).toUShort(&Ok);
		if (!Ok || static_cast<short>(Settings.Bots) < MinBots || Settings.Bots > MaxBots)
		{
			qDebug("Invalid number of bots.");
			return false;
		}
	}

	if (Parser.isSet(ResourcesOption))
	{
		Settings.Ressources = Parser.value(ResourcesOption).toUShort(&Ok);
		if (!Ok || static_cast<short>(Settings.Ressources) < MinRessources ||
				Settings.Ressources > MaxRessources)
		{
			qDebug("Invalid ressources level.");
			return false;
		}
	}

	if (Parser.isSet(MaxBlobsOption))
	{
		Settings.MaxBlobs = Parser.value(MaxBlobsOption).toULong(&Ok);
		if (!Ok || static_cast<short>(Settings.MaxBlobs) < MinMaxBlobs ||
				Settings.MaxBlobs > MaxMaxBlobs)
		{
			qDebug("Invalid maximum of blobs.");
			return false;
		}
	}

	if (Parser.isSet(ColorOption))
	{
		Settings.Color = Parser.value(ColorOption).toUShort(&Ok);
		if (!Ok || static_cast<short>(Settings.Color) < MinColor || Settings.Color > MaxColor)
		{
			qDebug("Invalid dish color index.");
			return false;
		}
	}

	return true;
}

//Captures interrupt and terminate signal and ends applications:
void Exit(int Signal)
{
	if (Signal == SIGINT || Signal == SIGTERM)
	{
		Application->exit();
		Server->disconnect();
		Game->disconnect();
		delete Server;
		delete Game;
	}
}

/***************************************************************************************************
**
** Copyright (c) 2019 Renan Sterle. All rights reserved.
**
** Redistribution and use in source and binary forms, with or without modification, are permitted
** provided that the following conditions are met:
**
**     1. Redistributions of source code must retain the above copyright notice, this list of
** conditions and the following disclaimer.
**     2. Redistributions in binary form must reproduce the above copyright notice, this list of
** conditions and the following disclaimer in the documentation and/or other materials provided with
** the distribution.
**     3. The name of the copyright holder may not be used to endorse or promote products derived
** from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE THE COPYRIGHT HOLDER "AS IS" AND ANY EXPRESS OR IMPLIED
** WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
** BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
***************************************************************************************************/

#include "Server.h"

//Constructs server:
Server_t::Server_t(Setting_t &Settings, QObject *Parent) : QTcpServer(Parent), Settings(Settings)
{
	//Initializes NextID;
	NextID = 0;

	//Finds address according to local or remote setting and sets server to listen:
	QHostAddress Address = QHostAddress::LocalHost;
	if (Settings.LocalMode)
	{
		if (!listen(QHostAddress::LocalHost)) throw(errorString().toStdString().c_str());
	}
	else
	{
		if (!listen(QHostAddress::Any)) throw(errorString().toStdString().c_str());

		QList<QHostAddress> Addresses = QNetworkInterface::allAddresses();
		for (QList<QHostAddress>::iterator It = Addresses.begin(); It != Addresses.end(); It++)
		{
			if (*It != QHostAddress::LocalHost)
			{
				Address = *It;
				break;
			}
		}
		if (Address == QHostAddress::LocalHost) throw("No connection.");
	}

	//Binds UDPSocket to predefined port number:
	UDPSocket = new QUdpSocket(this);
	if (!UDPSocket->bind(Settings.LocalMode ? QHostAddress::LocalHost: QHostAddress::Any,
						 Settings.Port))
	{
		throw("Could not bind UDP socket.");
	}

	//Reports TCP port to stdout and reports ports and adresses:
	std::cout << serverPort() << std::endl << std::flush;
	qDebug() << "Server started at: " << Address.toString()<< " and bound to: " << serverPort()
			 << "and to: " << UDPSocket->localPort();

	//Connects signals:
	connect(this, SIGNAL(newConnection()), this, SLOT(ListClient()));
	connect(UDPSocket, SIGNAL(readyRead()), this, SLOT(UDPAvailable()));
}

//Accepts new connection and lists peer as new client:
void Server_t::ListClient()
{
	QTcpSocket *Socket = nextPendingConnection();

	//Adds new client:
	short ID = NextID++;
	Clients[ID] = new Client_t(ID, Socket);

	//Sends greeting:
	Socket->write(ComposeServerGreeting(ID, UDPSocket->localPort()));

	//Connects signals:
	connect(Socket, SIGNAL(disconnected()), this, SLOT(RemoveClient()));
	connect(Socket, SIGNAL(error(QAbstractSocket::SocketError)),
			this, SLOT(SocketError(QAbstractSocket::SocketError)));
	connect(Socket, SIGNAL(readyRead()), this, SLOT(TCPAvailable()));
}

void Server_t::UDPAvailable()
{
	static QByteArray Data;
	static QHostAddress Sender;
	static ushort Port;

	//Appends all data available to UDP socket or treats discovery prompts:
	while (UDPSocket->hasPendingDatagrams())
	{
		//Check before if datagram is of discovery type, so that the sender is known:
		int Size = static_cast<int>(UDPSocket->pendingDatagramSize());
		Data.resize(Size);
		UDPSocket->readDatagram(Data.data(), Size, &Sender, &Port);

		uchar Type = *reinterpret_cast<uchar*>(Data.data());
		if (Type == Discovery)
		{
			//Replies sender server's informations:
			UDPSocket->writeDatagram(ComposeServerInfo(serverPort(), Settings.Name, Settings.Slots,
								static_cast<ushort>(Clients.size()), Settings.Bots), Sender, Port);
		}
		else
		{
			//Appends content:
			UDPBuffer.append(Data.data(), Size);
		}
	}

	//While buffer's length is enough to contain data length information...
	ushort Length;
	while (static_cast<ulong>(UDPBuffer.length()) >= sizeof(Length))
	{
		//Reads data length and processes if buffer is large enough:
		Length = *reinterpret_cast<ushort*>(UDPBuffer.data());
		if (UDPBuffer.length() >= Length+static_cast<ushort>(sizeof(Length)))
		{
			UDPBuffer.remove(0, sizeof(Length));
			QDataStream S(&UDPBuffer, QIODevice::ReadOnly);

			//Reads message type and acts accordingly:
			uchar Type;
			S >> Type;
			switch (Type)
			{
				case InputReport:
				{
					short ID;
					uchar Flags;
					QPoint Input;
					S >> ID >> Input >> Flags;
					if (Clients.find(ID) != Clients.end())
					{
						bool Split = Flags & SplitMask;
						bool Eject = Flags & EjectMask;
						emit UpdateInput(ID, Input, Split, Eject);
					}
					break;
				}
				default:
					break;
			}
			UDPBuffer.remove(0, Length);
		}
		else
		{
			break;
		}
	}
}

void Server_t::TCPAvailable()
{
	//Gets socket from sender:
	QTcpSocket *Socket = static_cast<QTcpSocket*>(sender());

	//Retrieves client ID and appends the data to the corresponding buffer:
	short ID;
	QByteArray *Buffer;
	if (FindClient(Socket, ID))
	{
		Buffer = Clients[ID]->GetBuffer();
		Buffer->append(Socket->readAll());
	}
	else
	{
		Socket->readAll();
		return;
	}

	//While buffer's length is enough to contain data length information...
	ushort Length;
	while (static_cast<ulong>(Buffer->length()) >= sizeof(Length))
	{
		//Reads data length and processes if buffer is large enough:
		Length = *reinterpret_cast<ushort*>(Buffer->data());		
		if (Buffer->length() >= Length+static_cast<ushort>(sizeof(Length)))
		{
			Buffer->remove(0, sizeof(Length));
			QDataStream S(Buffer, QIODevice::ReadOnly);

			//Reads message type and acts accordingly:
			uchar Type;
			S >> Type;
			switch (Type)
			{
				case ClientGreeting:
				{
					ushort ReceivedID, UDPPort;
					QString Name;
					QColor Color;
					S >> ReceivedID >> UDPPort >> Name >> Color;
					if (ID == ReceivedID)
					{
						Clients[ID]->SetUDPPort(UDPPort);
						emit JoinRequest(ID, Name, Color);
					}
					break;
				}
				default:
					break;
			}
			Buffer->remove(0, Length);
		}
		else
		{
			break;
		}
	}
}

//Finds client ID based on Socket:
bool Server_t::FindClient(QTcpSocket *Socket, short &ID)
{
	for (std::map<short, Client_t*>::iterator It = Clients.begin(); It != Clients.end(); It++)
	{
		if (It->second->GetSocket() == Socket)
		{
			ID = It->second->GetID();
			return true;
		}
	}
	return false;
}

//Sends frame to client corresponding to ID:
void Server_t::SendFrame(short ID, QByteArray &Frame)
{
	Client_t *Client = Clients[ID];
	UDPSocket->writeDatagram(AdjustFrame(Frame), Client->GetAddress(), Client->GetUDPPort());
}

//Reports socket errors:
void Server_t::SocketError(QAbstractSocket::SocketError)
{
	QTcpSocket *Socket = static_cast<QTcpSocket*>(sender());
	qDebug() << "Error: " << Socket->errorString();
}

//In case of disconnection, releases socket and informs game the client has been removed:
void Server_t::RemoveClient()
{
	QTcpSocket *Socket = static_cast<QTcpSocket*>(sender());

	Socket->disconnect();
	Socket->disconnectFromHost();
	Socket->deleteLater();

	short ID;
	if (FindClient(Socket, ID))
	{
		delete Clients[ID];
		Clients.erase(ID);
		emit ClientRemoved(ID);
	}
}

//Messages specific client:
void Server_t::Message(short ID, Type_t Type, const QString &Content)
{
	Clients[ID]->GetSocket()->write(ComposeMessage(Type, Content));
}

//Sends dish information to client:
void Server_t::SendDishParameters(short ID, Dish_t *Dish)
{
	Clients[ID]->GetSocket()->write(ComposeDishParameters(Dish->GetRadius(), Dish->GetColor()));
}

//Server destructor:
Server_t::~Server_t()
{
	for (std::map<short, Client_t*>::iterator It = Clients.begin(); It != Clients.end(); It++)
	{
		QTcpSocket *Socket = It->second->GetSocket();

		Socket->disconnect();
		Socket->disconnectFromHost();

		delete Socket;
		delete It->second;
	}
	UDPSocket->disconnect();
	delete UDPSocket;
}

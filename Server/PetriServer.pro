QT += gui
QT += network
CONFIG += c++11 console static optimize_full release
CONFIG -= app_bundle
DEFINES += QT_DEPRECATED_WARNINGS
SOURCES += \
    Client.cpp \
    Elements.cpp \
    Game.cpp \
    Main.cpp \
    Network.cpp \
    Player.cpp \
    RandomGenerator.cpp \
    Server.cpp

HEADERS += \
    Client.h \
    Common.h \
    Elements.h \
    Game.h \
    Main.h \
    Network.h \
    Player.h \
    RandomGenerator.h \
    Server.h

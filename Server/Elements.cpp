/***************************************************************************************************
**
** Copyright (c) 2019 Renan Sterle. All rights reserved.
**
** Redistribution and use in source and binary forms, with or without modification, are permitted
** provided that the following conditions are met:
**
**     1. Redistributions of source code must retain the above copyright notice, this list of
** conditions and the following disclaimer.
**     2. Redistributions in binary form must reproduce the above copyright notice, this list of
** conditions and the following disclaimer in the documentation and/or other materials provided with
** the distribution.
**     3. The name of the copyright holder may not be used to endorse or promote products derived
** from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE THE COPYRIGHT HOLDER "AS IS" AND ANY EXPRESS OR IMPLIED
** WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
** BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
***************************************************************************************************/

#include "Elements.h"
#include "Player.h"

//Dish constructor:
Dish_t::Dish_t(int Radius, const QColor &Color)
{
	this->Radius = Radius;
	this->Color = Color;
}

int Dish_t::GetRadius() const
{
	return Radius;
}

const QColor& Dish_t::GetColor() const
{
	return Color;
}

/*Creates new Blob from cell. It may be called in the simple mass ejection context or in the context
 * of a split. In the second case, the new blob is the base of the new cell:*/
Blob_t::Blob_t(Player_t *P, Cell_t *Cell, bool Ejection)
{
	if (Ejection)
	{
		Mass = Cell->InitialMass*EjectionRate;
		Cell->Gain(-Cell->InitialMass);
		Velocity = EjectionSpeed*Cell->Velocity.normalized();
		Center = QVector2D(Cell->GetCenter() + Cell->GetRadius()*
						   QVector2D(P->GetInput()-Cell->GetCenter()).normalized().toPoint());
	}
	else
	{
		Mass = Cell->GetMass()/2.0f;
		Cell->Gain(-Cell->GetMass()/2);
		Velocity = 4*Cell->Velocity;
		Cell->Velocity /= 2;
		Center = QVector2D(Cell->GetCenter() + 2*Cell->GetRadius()*
						   QVector2D(P->GetInput()-Cell->GetCenter()).normalized().toPoint());
	}
	UpdateRadius();
	Color = Cell->Color;
}

//Creates new blob with random color and random position:
Blob_t::Blob_t(int DishRadius, QRandomGenerator *Gen)
{
	Mass = InitialMass;
	UpdateRadius();
	Color = Colors[Gen->generate()%Colors.size()].second;

	double R = (DishRadius-static_cast<int>(Radius))*sqrt(Gen->generateDouble());
	double A = TwoPi*Gen->generateDouble();
	Center.setX(static_cast<int>((R*cos(A))));
	Center.setY(static_cast<int>(R*sin(A)));
	Velocity = Force = QVector2D(0, 0);
}

//Serializes blob reference:
QDataStream &operator<<(QDataStream &S, const Blob_t &Blob)
{
	S << static_cast<ushort>(Blob.Radius) << static_cast<uint>(Blob.Mass) << Blob.Center
	  << Blob.Velocity << Blob.Color;
	return S;
}

//Serializes blob by pointer:
QDataStream &operator<<(QDataStream &S, Blob_t *Blob)
{
	S << static_cast<ushort>(Blob->Radius) << static_cast<uint>(Blob->Mass) << Blob->Center
	  << Blob->Velocity << Blob->Color;
	return S;
}

//Initializes blob from stream:
QDataStream &operator>>(QDataStream &S, Blob_t &Blob)
{
	uint Mass;
	S >> Blob.Radius >> Mass >> Blob.Center >> Blob.Velocity >> Blob.Color;
	Blob.Mass = static_cast<int>(Mass);
	return S;
}

ushort Blob_t::GetRadius() const
{
	return Radius;
}

QPoint Blob_t::GetCenter() const
{
	return Center.toPoint();
}

const QColor& Blob_t::GetColor() const
{
	return Color;
}

int Blob_t::GetMass() const
{
	return static_cast<int>(Mass);
}

//Calculates radius according to mass:
void Blob_t::UpdateRadius()
{
	Radius = static_cast<ushort>(ceil(sqrt(2.0*static_cast<double>(AreaPerMass*Mass)/TwoPi)));
}

void Blob_t::Mark(bool NewMark)
{
	Marked = NewMark;
}

bool Blob_t::IsMarked()
{
	return Marked;
}

void Blob_t::SetForce(const QVector2D &Force)
{
	this->Force = Force;
}

//Moves blob limiting range considering dish radius and reduves velocity due to friction:
void Blob_t::Move(float DeltaT, int DishRadius, float Friction)
{
	Velocity += Force*DeltaT/sqrtf(Mass);
	Center += Velocity*DeltaT;
	if (Center.length()+Radius > DishRadius)
	{
		Center.normalize();
		Velocity.normalize();

		Velocity = Velocity - Center;

		Center *= DishRadius-Radius;
	}
	else
	{
		Velocity *= Friction;
	}
}

//Reduces mass according to toll parameter.
void Blob_t::DeductMass(float Toll)
{
	Mass *= powf(Toll, Mass/100);
}

void Blob_t::Gain(int M)
{
	Mass += M;
	UpdateRadius();
}

//Creates new cell due to split event:
Cell_t::Cell_t(Player_t *P, Cell_t *Cell) : Blob_t(P, Cell, false)
{
	ID = Cell->ID;
	Angle = Cell->Angle;
	Eccentricity = Cell->Eccentricity;
	DAngle = Cell->DAngle;
	Phase = Cell->Phase;
}

//Creates new cell and set random position:
Cell_t::Cell_t(short NewID, int DishRadius, QRandomGenerator *Gen, const QColor &NewColor)
	: Blob_t(DishRadius, Gen)
{
	ID = NewID;
	Color = NewColor;
	Mass = InitialMass;
	UpdateRadius();
	Angle = DAngle = Eccentricity = 0;
	Phase = TwoPi*Gen->generateDouble();

	double R = (DishRadius-static_cast<int>(Radius))*sqrt(Gen->generateDouble());
	double A = TwoPi*Gen->generateDouble();
	Center.setX(static_cast<int>((R*cos(A))));
	Center.setY(static_cast<int>(R*sin(A)));
}

double Cell_t::GetAngle() const
{
	return Angle;
}

double Cell_t::GetEccentricity() const
{
	return Eccentricity;
}

short Cell_t::GetID() const
{
	return ID;
}

//Integrates DAngle with respect to DDAngle and Angle with respect to DAngle.
void Cell_t::UpdateDAngle(double DDAngle)
{
	DAngle += DDAngle;
	Angle += DAngle;
}

//Calculates eccentricity:
void Cell_t::UpdateEccentricity(double Angle)
{
	Eccentricity = EccentricityF*sin(Angle+Phase);
}

//Serializes cell by pointer:
QDataStream &operator<<(QDataStream &S, Cell_t *Cell)
{
	S << Cell->ID << Cell->Angle << Cell->Eccentricity << static_cast<Blob_t*>(Cell);
	return S;
}

//Initializes cell from stream:
QDataStream &operator>>(QDataStream &S, Cell_t &Cell)
{
	S >> Cell.ID >> Cell.Angle >> Cell.Eccentricity >> static_cast<Blob_t&>(Cell);
	return S;
}

//Compares cells by mass:
bool CompareCells(Cell_t *A, Cell_t *B)
{
	return A->Mass < B->Mass;
}

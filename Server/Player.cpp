/***************************************************************************************************
**
** Copyright (c) 2019 Renan Sterle. All rights reserved.
**
** Redistribution and use in source and binary forms, with or without modification, are permitted
** provided that the following conditions are met:
**
**     1. Redistributions of source code must retain the above copyright notice, this list of
** conditions and the following disclaimer.
**     2. Redistributions in binary form must reproduce the above copyright notice, this list of
** conditions and the following disclaimer in the documentation and/or other materials provided with
** the distribution.
**     3. The name of the copyright holder may not be used to endorse or promote products derived
** from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE THE COPYRIGHT HOLDER "AS IS" AND ANY EXPRESS OR IMPLIED
** WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
** BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
***************************************************************************************************/

#include "Player.h"

//Constructs player:
Player_t::Player_t(short ID, int DishRadius, QRandomGenerator *Gen, const QColor &Color,
				   std::list<Cell_t*> &SortedCells) : SortedCells(SortedCells)
{
	this->ID = ID;
	this->Color = Color;
	this->DishRadius = DishRadius;
	Generator = Gen;

	Score = 0;
	Input = Center = QPoint(0, 0);
	Split = Eject = false;

	Spawn();
}

//Destroys player:
Player_t::~Player_t()
{
	for (std::list<Cell_t*>::iterator C = Cells.begin(); C != Cells.end(); C++)
	{
		SortedCells.erase(find(SortedCells.begin(), SortedCells.end(), *C));
		delete *C;
	}
}

void Player_t::SetNameBot(const QString &Name, bool Bot)
{
	this->Name = Name;
	this->Bot = Bot;
}

void Player_t::Spawn()
{
	Cell_t *Cell = new Cell_t(ID, DishRadius, Generator, Color);
	Cells.push_back(Cell);
	SortedCells.push_back(Cell);
	Score += Cell->GetMass();
}

short Player_t::GetID() const
{
	return ID;
}

std::list<Cell_t*> &Player_t::GetCells()
{
	return Cells;
}

ushort Player_t::GetCellsCount()
{
	return static_cast<ushort>(Cells.size());
}

bool ComparePlayers(Player_t *A, Player_t *B)
{
	return A->Score < B->Score;
}

void Player_t::SetScore(double Score)
{
	this->Score = Score;
}

double Player_t::GetScore()
{
	return Score;
}

double Player_t::GetRange()
{
	return RangeScoreFactor*sqrt(Score)/TwoPi;
}

QColor &Player_t::GetColor()
{
	return Color;
}

QString &Player_t::GetName()
{
	return Name;
}

QPoint &Player_t::GetCenter()
{
	return Center;
}

bool Player_t::IsBot()
{
	return Bot;
}

//Serializes pointer to player:
QDataStream &operator<<(QDataStream &DS, Player_t *S)
{
	DS << S->Color << S->Name << static_cast<ushort>(S->Score);
	return DS;
}

const QPoint &Player_t::GetInput() const
{
	return Input;
}

bool Player_t::ToEject()
{
	bool R = Eject;
	Eject = false;
	return R;
}

bool Player_t::ToSplit()
{
	bool R = Split;
	Split = false;
	return R;
}

void Player_t::UpdateInput(const QPoint &Input, bool Split, bool Eject)
{
	this->Input = Input;
	this->Split = Split;
	this->Eject = Eject;
}

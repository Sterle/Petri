/***************************************************************************************************
**
** Copyright (c) 2019 Renan Sterle. All rights reserved.
**
** Redistribution and use in source and binary forms, with or without modification, are permitted
** provided that the following conditions are met:
**
**     1. Redistributions of source code must retain the above copyright notice, this list of
** conditions and the following disclaimer.
**     2. Redistributions in binary form must reproduce the above copyright notice, this list of
** conditions and the following disclaimer in the documentation and/or other materials provided with
** the distribution.
**     3. The name of the copyright holder may not be used to endorse or promote products derived
** from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE THE COPYRIGHT HOLDER "AS IS" AND ANY EXPRESS OR IMPLIED
** WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
** BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
***************************************************************************************************/

#ifndef GAME_H
#define GAME_H

//Qt headers:
#include <QObject>
#include <QTimer>
#include <QString>
#include <QColor>
#include <QTime>
#include <QDebug>

//Compatibility headers:
#if QT_VERSION >= QT_VERSION_CHECK(5, 10, 0)
#include <QRandomGenerator>
#else
#include "RandomGenerator.h"
#endif

//Project headers:
#include "Elements.h"
#include "Common.h"
#include "Network.h"
#include "Player.h"

//Game class:
class Game_t : public QObject
{
		Q_OBJECT
	public:
		explicit Game_t(Setting_t &Settings, QObject *Parent = nullptr);
		~Game_t();
		void StartStop(bool Run);
	public slots:
		void Tick();
		void SpawnBlob();
		void JoinRequest(short ID, const QString &Name, const QColor &Color);
		void ClientRemoved(short ID);
		void UpdateInput(short ID, const QPoint &Input, bool Split, bool Eject);
	signals:
		void SendFrame(short ID, QByteArray &Frame);
		void Message(short ID, Type_t Type, const QString &Content);
		void SendDishParameters(short ID, Dish_t *Dish);
	private:
		typedef std::list<Cell_t*>::iterator CellsIterator;
		typedef std::list<Cell_t*>::reverse_iterator CellsReverseIterator;

		const int TickPeriod = 40;
		constexpr static float ForceFactor = 0.02f;
		constexpr static double MaxForce = 5;
		constexpr static double AttractionCoefficient = 0.3;
		constexpr static double RepulsionCoefficient = 0.25;
		constexpr static float MassToll = 0.99999f;
		constexpr static float VelocityDecay = 0.85f;
		constexpr static double AngleF = 0.04;
		constexpr static double DAngle = 0.2;
		constexpr static double CriticalMass = 1.5;
		constexpr static double EatingTolerance = 0.3;

		int Slots, Bots, BlobsPeriod;
		unsigned long MaxBlobs;

		QTimer *TickTimer, *BlobsTimer;
		QRandomGenerator *Gen;

		Dish_t *Dish;
		std::list<Blob_t> Blobs;
		std::map<short, Player_t*> Players;
		std::list<Player_t*> SortedPlayers;
		std::list<Cell_t*> SortedCells;

		void SpawnBots();
		void DecideInputs(Player_t *Bot);
		void EatBlobs();
		void EatCells();
		void CalculateForces(Player_t *P, std::list<Cell_t*> &Cells);
		void SplitCells(Player_t *P, std::list<Cell_t*> &Cells);
		void EjectBlobs(Player_t *P, std::list<Cell_t*> &Cells);
		void UpdateEffects(double DeltaT, double Angle);
		void BuildFrame(short ID, QByteArray &Frame);
		void SendBlobs(QDataStream &DS, const QPoint &Center, double Range);
		void SendCells(QDataStream &DS, const QPoint &Center, double Range);
		void Broadcast(Type_t Type, const QString &Content, bool Exclude = false, short ID = 0);
};

extern bool ComparePlayers(Player_t *A, Player_t *B);
extern bool CompareCells(Cell_t *A, Cell_t *B);

#endif



/***************************************************************************************************
**
** Copyright (c) 2019 Renan Sterle. All rights reserved.
**
** Redistribution and use in source and binary forms, with or without modification, are permitted
** provided that the following conditions are met:
**
**     1. Redistributions of source code must retain the above copyright notice, this list of
** conditions and the following disclaimer.
**     2. Redistributions in binary form must reproduce the above copyright notice, this list of
** conditions and the following disclaimer in the documentation and/or other materials provided with
** the distribution.
**     3. The name of the copyright holder may not be used to endorse or promote products derived
** from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE THE COPYRIGHT HOLDER "AS IS" AND ANY EXPRESS OR IMPLIED
** WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
** BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
***************************************************************************************************/

#ifndef PLAYER_H
#define PLAYER_H

//Qt headers:
#include <QColor>
#include <QPoint>
#include <Elements.h>

//Compatibility headers:
#if QT_VERSION >= QT_VERSION_CHECK(5, 10, 0)
#include <QRandomGenerator>
#else
#include "RandomGenerator.h"
#endif

//Player class:
class Player_t
{
	public:
		Player_t(short ID, int DishRadius, QRandomGenerator *Gen, const QColor &Color,
				 std::list<Cell_t*> &SortedCells);
		~Player_t();
		short GetID() const;
		void SetNameBot(const QString &Name, bool Bot);
		void Spawn();
		std::list<Cell_t*> &GetCells();
		ushort GetCellsCount();
		bool ToEject(), ToSplit();
		const QPoint &GetInput() const;
		void SetScore(double Score);
		double GetScore();
		double GetRange();
		void UpdateInput(const QPoint &Input, bool Split, bool Eject);
		QColor &GetColor();
		QString &GetName();
		QPoint &GetCenter();
		bool IsBot();
		friend bool ComparePlayers(Player_t* A, Player_t* B);
		friend QDataStream &operator<<(QDataStream &DS, Player_t *S);
	private:
		constexpr static double RangeScoreFactor = 600.0;

		int DishRadius;
		QRandomGenerator *Generator;
		bool Bot;
		short ID;
		double Score;
		QColor Color;
		QString Name;
		std::list<Cell_t*> Cells;
		std::list<Cell_t*> &SortedCells;
		QPoint Input, Center;
		bool Split, Eject;
};

#endif

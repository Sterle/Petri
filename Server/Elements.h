/***************************************************************************************************
**
** Copyright (c) 2019 Renan Sterle. All rights reserved.
**
** Redistribution and use in source and binary forms, with or without modification, are permitted
** provided that the following conditions are met:
**
**     1. Redistributions of source code must retain the above copyright notice, this list of
** conditions and the following disclaimer.
**     2. Redistributions in binary form must reproduce the above copyright notice, this list of
** conditions and the following disclaimer in the documentation and/or other materials provided with
** the distribution.
**     3. The name of the copyright holder may not be used to endorse or promote products derived
** from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE THE COPYRIGHT HOLDER "AS IS" AND ANY EXPRESS OR IMPLIED
** WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
** BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
***************************************************************************************************/

#ifndef ELEMENTS_H
#define ELEMENTS_H

//Qt headers:
#include <QtCore>
#include <QColor>
#include <QVector2D>

//Compatibility headers:
#if QT_VERSION >= QT_VERSION_CHECK(5, 10, 0)
#include <QRandomGenerator>
#else
#include "RandomGenerator.h"
#endif

//Project header:
#include "Common.h"

//Petri dish class:
class Dish_t
{
	public:
		Dish_t(int Radius, const QColor &Color);
		int GetRadius() const;
		const QColor &GetColor() const;
	private:
		int Radius;
		QColor Color;
};

//Blob class:
class Cell_t;
class Player_t;
class Blob_t
{
	public:
		Blob_t(){}
		Blob_t(Player_t *P, Cell_t *Cell, bool Ejection);
		Blob_t(int DishRadius, QRandomGenerator *Gen);
		ushort GetRadius() const;
		QPoint GetCenter() const;
		const QColor& GetColor() const;
		int GetMass() const;
		void SetForce(const QVector2D &Force);
		void Move(float DeltaT, int DishRadius, float Friction);
		void DeductMass(float Toll);
		void Gain(int M);
		void UpdateRadius();
		void Mark(bool NewMark);
		bool IsMarked();
	private:
		const static int AreaPerMass = 64;
		constexpr static float InitialMass = 1.0f;
		constexpr static double EjectionSpeed = 40;
		constexpr static double EjectionRate = 0.6;
		bool Marked;
	protected:
		QVector2D Center, Force, Velocity;
		float Mass;
		ushort Radius;
		QColor Color;

		friend QDataStream &operator<<(QDataStream &S, const Blob_t &Blob);
		friend QDataStream &operator<<(QDataStream &S, Blob_t *Blob);
		friend QDataStream &operator>>(QDataStream &S, Blob_t &Blob);
};

//Cell class:
class Cell_t : public Blob_t
{
	public:
		const static int InitialMass = 20;

		Cell_t() : Blob_t(){}
		Cell_t(Player_t *P, Cell_t *Cell);
		Cell_t(short ID, int DishRadius, QRandomGenerator *Gen, const QColor &Color);

		double GetAngle() const;
		double GetEccentricity() const;
		short GetID() const;
		void UpdateDAngle(double DDAngle);
		void UpdateEccentricity(double Angle);
	private:
		constexpr static double EccentricityF = 0.05;

		short ID;

		double Angle = 0, Eccentricity = 0;
		double DAngle = 0, Phase = 0;

		friend QDataStream &operator<<(QDataStream &S, Cell_t *Cell);
		friend QDataStream &operator>>(QDataStream &S, Cell_t &Cell);
		friend bool CompareCells(Cell_t *A, Cell_t *B);
};

#endif

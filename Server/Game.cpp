/***************************************************************************************************
**
** Copyright (c) 2019 Renan Sterle. All rights reserved.
**
** Redistribution and use in source and binary forms, with or without modification, are permitted
** provided that the following conditions are met:
**
**     1. Redistributions of source code must retain the above copyright notice, this list of
** conditions and the following disclaimer.
**     2. Redistributions in binary form must reproduce the above copyright notice, this list of
** conditions and the following disclaimer in the documentation and/or other materials provided with
** the distribution.
**     3. The name of the copyright holder may not be used to endorse or promote products derived
** from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE THE COPYRIGHT HOLDER "AS IS" AND ANY EXPRESS OR IMPLIED
** WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
** FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
** BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
***************************************************************************************************/

#include "Game.h"

//Game constructor:
Game_t::Game_t(Setting_t &Settings, QObject *Parent) : QObject(Parent)
{
	//Initialize members:
	Slots = Settings.Slots;
	Bots = Settings.Bots;
	BlobsPeriod = 60000/Settings.Ressources;
	MaxBlobs = Settings.MaxBlobs;

	//Creates petri dish:
	Dish = new Dish_t(Settings.Radius, Colors[static_cast<ulong>(Settings.Color)].second);
	//Creates and feeds random number generator:
	Gen = new QRandomGenerator(static_cast<uint>(QTime::currentTime().msec()));

	//Starts and connects timers:
	TickTimer = new QTimer;
	connect(TickTimer, SIGNAL(timeout()), this, SLOT(Tick()));
	BlobsTimer = new QTimer;
	connect(BlobsTimer, SIGNAL(timeout()), this, SLOT(SpawnBlob()));

	//Spawns Bots:
	SpawnBots();
}

void Game_t::SpawnBots()
{
	//Bots have negative IDs.
	for (short ID = -1; ID >= -Bots; ID--)
	{
		const QColor &Color = Colors[Gen->generate()%Colors.size()].second;
		Player_t *P = new Player_t(ID, Dish->GetRadius(), Gen, Color, SortedCells);
#if QT_VERSION > QT_VERSION_CHECK(5, 5, 1)
		P->SetNameBot(QString("🤖")+QString::number(-ID), true);
#else
		P->SetNameBot(QString("Bot")+QString::number(-ID), true);
#endif
		Players[ID] = P;
		SortedPlayers.push_back(P);
	}
}

void Game_t::StartStop(bool Run)
{
	if (Run)
	{
		if (!TickTimer->isActive())
		{
			TickTimer->start(TickPeriod);
			BlobsTimer->start(BlobsPeriod);
		}
	}
	else
	{
		TickTimer->stop();
		BlobsTimer->stop();
	}
}

void Game_t::Tick()
{
	static double Angle = 0;
	static QTime Last = QTime::currentTime();
	QTime Now = QTime::currentTime();
	float DeltaT = Last.msecsTo(QTime::currentTime())/40.0f;
	Last = Now;

	//Sorts SortedSubjects:
	SortedPlayers.sort(ComparePlayers);

	//Increments angle:
	Angle += DAngle*static_cast<double>(DeltaT);

	//Operations for all cells:
	EatBlobs();
	EatCells();

	//Operations for each player:
	for (std::list<Player_t*>::iterator P = SortedPlayers.begin(); P != SortedPlayers.end(); P++)
	{
		if (!(*P)->GetCellsCount())
		{
			if (!(*P)->IsBot())
			{
				emit Message((*P)->GetID(), Status, "You were extinct!");
			}
			(*P)->Spawn();
		}

		//Decide bot's movements:
		if ((*P)->IsBot())
		{
			DecideInputs(*P);
		}

		std::list<Cell_t*> &Cells = (*P)->GetCells();
		SplitCells(*P, Cells);
		EjectBlobs(*P, Cells);

		CalculateForces(*P, Cells);
	}

	//Moves all blobs:
	float Friction = powf(VelocityDecay, DeltaT);
	for (std::list<Blob_t>::iterator B = Blobs.begin(); B != Blobs.end(); B++)
	{
		B->Move(DeltaT, Dish->GetRadius(), Friction);
	}
	//Moves all cells:
	for (CellsIterator C = SortedCells.begin(); C != SortedCells.end(); C++)
	{
		(*C)->Move(DeltaT, Dish->GetRadius(), Friction);
	}

	//Deducts mass:
	float Toll = powf(MassToll, DeltaT);
	for (CellsIterator C = SortedCells.begin(); C != SortedCells.end(); C++)
	{
		(*C)->DeductMass(Toll);
	}

	//Operations for all cells:
	UpdateEffects(static_cast<double>(DeltaT), Angle);
	SortedCells.sort(CompareCells);

	//Sends frame to all players:
	for (std::list<Player_t*>::iterator P = SortedPlayers.begin(); P != SortedPlayers.end(); P++)
	{
		if (!(*P)->IsBot())
		{
			QByteArray Frame;
			short ID = (*P)->GetID();
			BuildFrame(ID, Frame);
			emit SendFrame(ID, Frame);
		}
	}
}

void Game_t::DecideInputs(Player_t *Bot)
{
	//Bot seekes closest blob or edible rival:
	QPoint Input(0, 0), &Center = Bot->GetCenter(), P;
	if (Blobs.size())
	{
		double BestGrade = -1, Grade, R;
		for (CellsIterator C = SortedCells.begin(); C != SortedCells.end(); C++)
		{
			//Must be a rival and within critical mass margin:
			if ((*C)->GetID() != Bot->GetID() && Bot->GetScore()/static_cast<double>((*C)->GetMass()) > CriticalMass)
			{
				P = Center-(*C)->GetCenter();
				R = P.manhattanLength();
				Grade = (*C)->GetMass()/fmax(1.0, R);
				if (Grade > BestGrade)
				{
					BestGrade = Grade;
					Input = 2*(*C)->GetCenter()-Center;
				}
			}
		}
		for (std::list<Blob_t>::const_iterator B = Blobs.begin(); B != Blobs.end(); B++)
		{
			P = Center-B->GetCenter();
			R = P.manhattanLength();
			Grade = B->GetMass()/fmax(1.0, R);
			if (Grade > BestGrade)
			{
				BestGrade = Grade;
				Input = 2*B->GetCenter()-Center;
			}
		}
	}
	Bot->UpdateInput(Input, false, false);
}

/*Iterates through all cells and checks for all blobs if they're in eating range and above the mass
 * threshold. If yes, the blob is eaten and it's mass is incorporated by the eater. In case the
 * number of blobs drops below the maximum the spawn timer is disabled, it is the restarted.*/
void Game_t::EatBlobs()
{
	for (CellsIterator C = SortedCells.begin(); C != SortedCells.end(); C++)
	{
		for (std::list<Blob_t>::iterator B = Blobs.begin(); B != Blobs.end(); B++)
		{
			if ((*C)->GetMass()/static_cast<double>(B->GetMass()) > CriticalMass)
			{
				if (static_cast<double>(QVector2D(B->GetCenter() - (*C)->GetCenter()).length()) <=
						abs((*C)->GetRadius()-EatingTolerance*B->GetRadius()))
				{
					(*C)->Gain(B->GetMass());
					Blobs.erase(B--);
				}
			}
		}
	}
	if (Blobs.size() < MaxBlobs && !BlobsTimer->isActive())
	{
		BlobsTimer->start(BlobsPeriod);
	}
}

/*Iterates through SortedCells in reverse direction, from the heaviest to lightes cells and checks
 * if the larger is capable of eating any of the smaller cells. If yes, the mass from the one eaten
 * is incorporated by the eater, the eaten is removed and the iteration is restarted.*/
void Game_t::EatCells()
{
	//Iterates from heaviest cells to lightest:
	for (CellsReverseIterator CA = SortedCells.rbegin(); CA != SortedCells.rend(); CA++)
	{
		for (CellsReverseIterator CB = std::next(CA); CB != SortedCells.rend(); CB++)
		{
			//Observes critical mass ratio:
			if ((*CA)->GetMass()/static_cast<double>((*CB)->GetMass()) > CriticalMass)
			{
				//If overlapping in considerable, the smaller cell is eaten:
				if (static_cast<double>(QVector2D((*CA)->GetCenter() - (*CB)->GetCenter()).length())
						<= (*CA)->GetRadius()-EatingTolerance*(*CB)->GetRadius())
				{
					(*CA)->Gain((*CB)->GetMass());
					Player_t *P = Players[(*CB)->GetID()];
					std::list<Cell_t*> &Cells = P->GetCells();
					delete *CB;

					Cells.erase(find(Cells.begin(), Cells.end(), *CB));
					SortedCells.erase(std::next(CB).base());

					CA = SortedCells.rbegin();
					CB = std::next(CA);
				}
			}
		}
	}
}

//Updates animation effects:
void Game_t::UpdateEffects(double DeltaT, double Angle)
{
	for (CellsIterator C = SortedCells.begin(); C != SortedCells.end(); C++)
	{
		(*C)->UpdateDAngle(DeltaT*AngleF*(Gen->generateDouble()-0.5));
		(*C)->UpdateEccentricity(Angle);
	}
}

//Splits once each cell over minimum mass of a player:
void Game_t::SplitCells(Player_t *P, std::list<Cell_t*> &Cells)
{
	if (P->ToSplit())
	{
		std::vector<Cell_t*> NewCells;
		for (CellsIterator C = Cells.begin(); C != Cells.end(); C++)
		{
			if ((*C)->GetMass() > 2*(*C)->InitialMass)
			{
				NewCells.push_back(new Cell_t(P, *C));
			}
		}
		for (std::vector<Cell_t*>::iterator C = NewCells.begin(); C != NewCells.end(); C++)
		{
			Cells.push_back(*C);
			SortedCells.push_back(*C);
		}
		NewCells.clear();
	}
}

//Ejects mass from each cell over minimum mass of a player:
void Game_t::EjectBlobs(Player_t *P, std::list<Cell_t*> &Cells)
{
	if (P->ToEject())
	{
		for (CellsIterator C = Cells.begin(); C != Cells.end(); C++)
		{
			if ((*C)->GetMass() > 2*(*C)->InitialMass)
			{
				Blobs.push_back(Blob_t(P, *C, true));
			}
		}
	}
}

void Game_t::CalculateForces(Player_t *P, std::list<Cell_t*> &Cells)
{
	//Calculates colony's center of mass and total score:
	double Score = 0;
	QPoint &Center = P->GetCenter();
	Center = QPoint(0, 0);
	for (CellsIterator C = Cells.begin(); C != Cells.end(); C++)
	{
		Center += (*C)->GetMass()*(*C)->GetCenter();
		Score += (*C)->GetMass();
	}
	Center /= static_cast<double>(Score);
	P->SetScore(Score);

	//For each cell of colony...
	for (CellsIterator CA = Cells.begin(); CA != Cells.end(); CA++)
	{
		//Calculates force due to input:
		QVector2D Force(P->GetInput()-Center);
		Force *= ForceFactor;
		if (static_cast<double>(Force.length()) >= MaxForce)
		{
			Force.normalize();
			Force *= MaxForce;
		}

		//Calculates force due to attraction to each other cell in colony:
		for (CellsIterator CB = Cells.begin(); CB != Cells.end(); CB++)
		{
			QVector2D V((*CB)->GetCenter()-(*CA)->GetCenter());
			double Radii = (*CA)->GetRadius() + (*CB)->GetRadius();
			if (CB != CA)
			{
				//There is attraction between all cells:
				if (static_cast<double>(V.length()) > Radii)
				{
					Force += static_cast<float>(AttractionCoefficient) *
							 V.normalized()*sqrtf(V.length());
				}
				//There is repulsion between overlapping cells of comparable size:
				if (V.length() > 0 && fmax((*CA)->GetMass(), (*CB)->GetMass())/
						fmin((*CA)->GetMass(), (*CB)->GetMass()) < CriticalMass)
				{
					Force -= static_cast<float>(RepulsionCoefficient) *
							 V.normalized()*sqrtf(V.length());
				}
			}
		}
		(*CA)->SetForce(Force);
	}
}

void Game_t::BuildFrame(short ID, QByteArray &Frame)
{
	QDataStream DS(&Frame, QIODevice::WriteOnly);

	//Sends center and zoom:
	Player_t *P = Players[ID];
	QPoint &Center = P->GetCenter();
	double Range = P->GetRange();
	DS << Range << Center;

	//Sends all subjects:
	DS << static_cast<ushort>(Players.size());
	for (std::map<short, Player_t*>::iterator P = Players.begin(); P != Players.end(); P++)
	{
		DS << P->first << P->second;
	}

	SendBlobs(DS, Center, Range);
	SendCells(DS, Center, Range);

	//Compresses frame:
	Frame = qCompress(Frame, 9);
}

void Game_t::SendBlobs(QDataStream &DS, const QPoint &Center, double Range)
{
	ushort NBlobs = 0;
	//Marks blobs according to range:
	for (std::list<Blob_t>::iterator B = Blobs.begin(); B != Blobs.end(); B++)
	{
		if (QPoint(B->GetCenter() - Center).manhattanLength() <= Range+B->GetRadius())
		{
			B->Mark(true);
			NBlobs++;
		}
		else
		{
			B->Mark(false);
		}
	}

	//Sends all marked blobs:
	DS << NBlobs;
	for (std::list<Blob_t>::iterator B = Blobs.begin(); B != Blobs.end(); B++)
	{
		if (B->IsMarked()) DS << *B;
	}
}

void Game_t::SendCells(QDataStream &DS, const QPoint &Center, double Range)
{
	ushort NCells = 0;
	//Marks cells according to range:
	for (CellsIterator C = SortedCells.begin(); C != SortedCells.end(); C++)
	{
		if (QPoint((*C)->GetCenter() - Center).manhattanLength() <= Range+(*C)->GetRadius())
		{
			(*C)->Mark(true);
			NCells++;
		}
		else
		{
			(*C)->Mark(false);
		}
	}

	//Sends all marked cells:
	DS << static_cast<ushort>(SortedCells.size());
	for (CellsIterator C = SortedCells.begin(); C != SortedCells.end(); C++)
	{
		if ((*C)->IsMarked()) DS << *C;
	}
}

//Spawns new blob and disables spawning, should the number of blobs become too high.
void Game_t::SpawnBlob()
{
	Blobs.push_back(Blob_t(Dish->GetRadius(), Gen));
	if (Blobs.size() >= MaxBlobs)
	{
		BlobsTimer->stop();
	}
}

//Adds player to game if slot is available:
void Game_t::JoinRequest(short ID, const QString &Name, const QColor &Color)
{
	if (Slots+Bots > static_cast<ushort>(Players.size()))
	{
		qDebug() << "Join request accepted: " << ID << Name << Color;
		Player_t *P = new Player_t(ID, Dish->GetRadius(), Gen, Color, SortedCells);
		P->SetNameBot(Name, false);
		Players[ID] = P;
		SortedPlayers.push_back(P);
		emit Message(ID, Success, "Join accepted.");
		Broadcast(Status, P->GetName() + " joined.", true, ID);
		emit SendDishParameters(ID, Dish);

		StartStop(true);
	}
	else
	{
		qDebug() << "Join request denied: " << ID << Name << Color;
		emit Message(ID, Failure, "Join denied.");
	}
}

//Removes player from game:
void Game_t::ClientRemoved(short ID)
{
	std::map<short, Player_t*>::iterator I = Players.find(ID);
	if (I != Players.end())
	{
		Player_t *P = I->second;
		Players.erase(ID);
		std::list<Player_t*>::iterator I = find(SortedPlayers.begin(), SortedPlayers.end(), P);
		SortedPlayers.erase(I);
		QString Message = P->GetName() + " left.";
		delete P;
		if (!Players.size())
		{
			StartStop(false);
		}
		qDebug() << Message;
		Broadcast(Status, Message);
	}
}

//If input report corresponds to a valid player, updates it input:
void Game_t::UpdateInput(short ID, const QPoint &Input, bool Split, bool Eject)
{
	std::map<short, Player_t*>::iterator P = Players.find(ID);
	if (P != Players.end())
	{
		P->second->UpdateInput(Input, Split, Eject);
	}
}

Game_t::~Game_t()
{
	//Deletes timers:
	TickTimer->disconnect();
	TickTimer->stop();
	delete TickTimer;

	BlobsTimer->disconnect();
	BlobsTimer->stop();
	delete BlobsTimer;

	//Deletes dish:
	delete Dish;

	//Deletes generator:
	delete Gen;

	//Deletes remaining clients:
	for (std::map<short, Player_t*>::iterator P = Players.begin(); P != Players.end(); P++)
	{
		delete P->second;
	}
}

//Sends message to all non-excluded players:
void Game_t::Broadcast(Type_t Type, const QString &Content, bool Exclude, short ID)
{
	for (std::map<short, Player_t*>::iterator P = Players.begin(); P != Players.end(); P++)
	{
		if (!P->second->IsBot() && !(Exclude && ID == P->second->GetID()))
		{
			emit Message(P->second->GetID(), Type, Content);
		}
	}
}
